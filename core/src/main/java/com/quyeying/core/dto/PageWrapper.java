package com.quyeying.core.dto;

import com.github.pagehelper.Page;

import java.util.List;

/**
 * 分页结果包裹
 * Created by ben on 2016/5/8/008.
 */
public class PageWrapper {
  /**
   * 页码，从1开始
   */
  private int pageNum;
  /**
   * 页面大小
   */
  private int pageSize;
  /**
   * 总数
   */
  private long total;
  /**
   * 总页数
   */
  private int pages;

  private List data;

  public int getPageNum() {
    return pageNum;
  }

  public void setPageNum(int pageNum) {
    this.pageNum = pageNum;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public long getTotal() {
    return total;
  }

  public void setTotal(long total) {
    this.total = total;
  }

  public int getPages() {
    return pages;
  }

  public void setPages(int pages) {
    this.pages = pages;
  }

  public List getData() {
    return data;
  }

  public void setData(List data) {
    this.data = data;
  }


  public static PageWrapper fromPage(Page page){
    PageWrapper result = new PageWrapper();

    result.setData(page.getResult());
    result.setPageNum(page.getPageNum());
    result.setPages(page.getPages());
    result.setPageSize(page.getPageSize());
    result.setTotal(page.getTotal());
    return result;
  }

}
