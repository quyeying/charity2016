package com.quyeying.core.dto;

/**
 * Created by bysun on 2016/5/15.
 */
public class Combox {
  Object pkid;
  String valueName;

  public Object getPkid() {
    return pkid;
  }

  public void setPkid(Object pkid) {
    this.pkid = pkid;
  }

  public String getValueName() {
    return valueName;
  }

  public void setValueName(String valueName) {
    this.valueName = valueName;
  }
}
