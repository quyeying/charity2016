package com.quyeying.core.dto;

import java.io.Serializable;

/**
 * 分页查询数据传输对象
 * <p>
 * Created by Watson W on 2016/4/23.
 */
public class PageParams implements Serializable {

  private static final long serialVersionUID = -7104268300142170638L;

  private Integer page;

  private Integer pageSize;

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

}
