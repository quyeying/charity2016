package com.quyeying.core.listener;

import com.google.common.base.Strings;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Session Listener
 * <p>
 * Created by Watson W on 2016/4/9.
 */
public class SessionListener implements HttpSessionListener {


  @Override
  public void sessionCreated(HttpSessionEvent httpSessionEvent) {
    ServletContext ctx = httpSessionEvent.getSession().getServletContext();
    String timeout = ctx.getInitParameter("sessionTimeout");
    int maxInactiveInterval = 20 * 60;
    if (!Strings.isNullOrEmpty(timeout)) {
      try {
        maxInactiveInterval = Integer.parseInt(timeout);
      } catch (NumberFormatException e) {
        maxInactiveInterval = 20 * 60;
      }
    }
    // 设置session 超时时间 20分钟
    httpSessionEvent.getSession().setMaxInactiveInterval(maxInactiveInterval);
  }

  @Override
  public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

  }

}
