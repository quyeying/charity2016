package com.quyeying.core;

/**
 * 常用常量定义
 */
public interface Const {

  public static String OPENID_SESSION_KEY = "O_SID";
  public static String OPENID_SESSION_VALID = "O_NID";
  public static String ENCODING = "UTF-8";

  public static byte[] OPENID_AES_KEY = "6wHG5FMoAkbK^WkDGu6MH%fJ*#ano@Cd".getBytes();
  public static byte[] VI = "9Sl*Ee9n3&C!LCqL".getBytes();

  int YES = 1;
  int NO = 0;

  String YES_STR = "1";
  String NO_STR = "0";


  String SUCCESS_STR = "success";
  String FAILURE_STR = "failure";

  String encode = "UTF-8";
}
