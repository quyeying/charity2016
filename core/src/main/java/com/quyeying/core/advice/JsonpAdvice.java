package com.quyeying.core.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

/**
 * JsonpAdvice jsonp 支持
 * <p>
 * Created by Watson W on 2016/4/29.
 */
@ControllerAdvice(annotations = EnableJsonpAdvice.class)
public class JsonpAdvice extends AbstractJsonpResponseBodyAdvice {

  public JsonpAdvice() {
    super("callback");
  }

}
