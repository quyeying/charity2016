package com.quyeying.core.advice;

import java.lang.annotation.*;

/**
 * EnableJsonpAdvice
 * <p>
 * Created by Watson W on 2016/4/29.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableJsonpAdvice {

}
