package com.quyeying.core.security;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Security Utils
 * <p>
 * Created by Watson W on 2016/4/19.
 */
public class SessionUtils {

  /**
   * 日志
   */
  private static final Logger log = LoggerFactory.getLogger(SessionUtils.class);

  /**
   * session 中当前用户key
   */
  public static final String CURRENT_USER = "CURRENT_USER";

  private SessionUtils() {
  }

  /**
   * 注册当前用户
   *
   * @param account 用户
   */
  public static void setCurrentUser(Account account) {
    if (null != account) {
      setSession(CURRENT_USER, account);
    }
  }

  /**
   * 获取当前用户
   *
   * @return 当前登录用户
   */
  public static Account getCurrentUser() {
    Subject currentUser = SecurityUtils.getSubject();
    if (null != currentUser) {
      Session session = currentUser.getSession();
      if (null != session) {
        Account account = (Account) session.getAttribute(CURRENT_USER);
        if (null != account) {
          return account;
        }
      }
    }
    return null;
  }

  /**
   * 移除当前登录用户
   */
  public static void removeCurrentUser() {
    Subject currentUser = SecurityUtils.getSubject();
    if (null != currentUser) {
      Session session = currentUser.getSession();
      session.removeAttribute(CURRENT_USER);
    }
  }


  /**
   * 将一些数据放到ShiroSession中,以便于其它地方使用
   * 比如Controller,使用时直接用HttpSession.getAttribute(key)就可以取到
   *
   * @param key   session key
   * @param value session value
   */
  public static void setSession(Object key, Object value) {
    Subject currentUser = SecurityUtils.getSubject();
    if (null != currentUser) {
      Session session = currentUser.getSession();
      log.debug("Shiro 的 Session默认超时时间为[" + session.getTimeout() + "]毫秒");
      session.setAttribute(key, value);
    }
  }

}
