package com.quyeying.core.security;

import com.google.common.base.Strings;
import org.apache.shiro.config.Ini;

import java.text.MessageFormat;
import java.util.List;

/**
 * ShiroMetaSource
 * 获取所有权限
 * <p>
 * Created by Watson W on 2016/4/19.
 */
public class ShiroMetaSource {

  private static final String PREMISSION_STRING = "user,setSession,roleOr[{0},SUPER_ADMIN]";

  private IShiroUserService shiroUserService;

  private String head = "";

  private String tail = "";

  public ShiroMetaSource(IShiroUserService shiroUserService, String head, String tail) {
    this.shiroUserService = shiroUserService;
    this.head = head;
    this.tail = tail;
  }

  public ShiroMetaSource(IShiroUserService shiroUserService) {
    this.shiroUserService = shiroUserService;
  }

  public Ini.Section getObject() {

    List<? extends Permission> permissions = shiroUserService.findAllPermission();

    Ini ini = new Ini();
    //加载默认的url
    ini.load(head);
    Ini.Section section = ini.getSection(Ini.DEFAULT_SECTION_NAME);

    permissions.stream()
      .filter(permission -> !Strings.isNullOrEmpty(permission.getMenuPath()))
      .forEach(permission -> section.put(
        permission.getMenuPath(),
        MessageFormat.format(PREMISSION_STRING, permission.getMenuSign())
      ));

    ini = new Ini();
    ini.load(tail);
    section.putAll(ini.getSection(Ini.DEFAULT_SECTION_NAME));

    return section;
  }
}
