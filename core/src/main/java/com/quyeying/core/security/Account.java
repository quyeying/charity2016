package com.quyeying.core.security;

/**
 * 用户session对象
 * Created by Watson W on 2016/4/19.
 */
public interface Account {

  Long getPkid();

  String getUserName();

  String getPassWord();

  String getNikeName();

  String getUserGroup();
}
