package com.quyeying.core.security;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Created by ben on 2016/5/3/003.
 */
public class RememberMetFilter extends AccessControlFilter {

  @Autowired
  private IShiroUserService service;

  @Override
  public boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
    Subject subject = getSubject(request, response);
    if (subject == null) {
      return false;
    }
    Account account = SessionUtils.getCurrentUser();
    //判断session是否失效，若失效刷新之
    if (account == null) {
      String username = (String) subject.getPrincipal();
      Account user = service.findAccountByName(username);
      SessionUtils.setCurrentUser(user);
    }
    return true;
  }

  @Override
  protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
    return true;
  }

  @Override
  protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
    return true;
  }
}
