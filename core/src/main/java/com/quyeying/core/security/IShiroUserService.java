package com.quyeying.core.security;


import java.util.List;

/**
 * shiro service
 * <p>
 * Created by Watson W on 2016/4/19.
 */
public interface IShiroUserService {

  /**
   * 获取所有权限
   *
   * @return 所有权限
   */
  List<? extends Permission> findAllPermission();

  /**
   * 获取用户权限标识符
   *
   * @return 权限标识符
   */
  List<String> findPermissionsByAccount(Long pkid);

  /**
   * 根据用户名获取用户
   *
   * @param name 用户名
   * @return 用户
   */
  Account findAccountByName(String name);

}
