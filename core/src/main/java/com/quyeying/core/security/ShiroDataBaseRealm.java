package com.quyeying.core.security;

import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * Shiro DataBase Realm
 * 设置用户角色和权限 登录认证账号密码
 * <p>
 * Created by Watson W on 2016/4/19.
 */
public class ShiroDataBaseRealm extends AuthorizingRealm {

  private IShiroUserService shiroUserService;

  public ShiroDataBaseRealm(IShiroUserService shiroUserService) {
    this.shiroUserService = shiroUserService;
  }

  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
    Account account = SessionUtils.getCurrentUser();
    // 为当前用户设置角色和权限
    SimpleAuthorizationInfo simpleAuthorInfo = new SimpleAuthorizationInfo();
    if (null != account) {
      if ("admin".equals(account.getUserName()))
        simpleAuthorInfo.addRole("SUPER_ADMIN");

      // 添加用户所有权限
      simpleAuthorInfo.addRoles(shiroUserService.findPermissionsByAccount(account.getPkid()));
    }
    return simpleAuthorInfo;
  }

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) {
    UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
    Account account = shiroUserService.findAccountByName(token.getUsername());

    if (account == null) {
      throw new UnknownAccountException();//没找到帐号
    }

    SessionUtils.setCurrentUser(account);
    return new SimpleAuthenticationInfo(
      account.getUserName(), //用户名
      account.getPassWord(), //密码
      getName()  //realm name
    );
  }
}
