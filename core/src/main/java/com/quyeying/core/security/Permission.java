package com.quyeying.core.security;

/**
 * 权限
 * Created by Watson W on 2016/4/19.
 */
public interface Permission {

  String getMenuSign();

  String getMenuPath();

}
