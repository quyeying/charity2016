package com.quyeying.core.converter;

import com.google.common.base.Strings;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;

/**
 * spring 时间转换器
 * Created by Watson W on 2016/4/9.
 */
public class DateConverter implements Converter<String, Date> {

  private static final DateTimeFormatter DATE = DateTimeFormat.forPattern("yyyy-MM-dd");
  private static final DateTimeFormatter DATE_TIME = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

  @Override
  public Date convert(String s) {
    if (Strings.isNullOrEmpty(s)) {
      return null;
    } else if (10 == s.length()) {
      return DateTime.parse(s, DATE).toDate();
    } else if (19 == s.length()) {
      return DateTime.parse(s, DATE_TIME).toDate();
    } else {
      return null;
    }
  }

}
