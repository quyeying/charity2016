package com.quyeying.core.mapper;

/**
 * Created by ben on 2016/8/5.
 */
public class TestDto {

  private String name;
  private Integer age;
  private String nick;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public String getNick() {
    return nick;
  }

  public void setNick(String nick) {
    this.nick = nick;
  }


  public static final class Builder {
    private String name;
    private Integer age;
    private String nick;

    private Builder() {
    }

    public static Builder builder() {
      return new Builder();
    }

    public Builder withName(String name) {
      this.name = name;
      return this;
    }

    public Builder withAge(Integer age) {
      this.age = age;
      return this;
    }

    public Builder withNick(String nick) {
      this.nick = nick;
      return this;
    }

    public TestDto build() {
      TestDto testDto = new TestDto();
      testDto.setName(name);
      testDto.setAge(age);
      testDto.setNick(nick);
      return testDto;
    }
  }

  @Override
  public String toString() {
    return "TestDto{" +
      "name='" + name + '\'' +
      ", age=" + age +
      ", nick='" + nick + '\'' +
      '}';
  }
}
