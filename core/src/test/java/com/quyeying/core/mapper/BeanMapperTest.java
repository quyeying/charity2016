package com.quyeying.core.mapper;

import org.junit.Assert;

/**
 * Created by ben on 2016/8/5.
 */
public class BeanMapperTest {
  @org.junit.Test
  public void copy() throws Exception {
    TestDto a = TestDto.Builder.builder().withAge(1).withName("aa").build();
    TestDto b = TestDto.Builder.builder().withAge(2).withName("bb").withNick("bbb").build();
    BeanMapper.copy(a,b);
    Assert.assertNotNull(b.getNick());
    Assert.assertEquals("a和b的age应当相同",a.getAge(),b.getAge());
    Assert.assertEquals("a和b的name应当相同",a.getName(),b.getName());
    Assert.assertNotEquals("a和b的nick应当不相同",a.getNick(),b.getNick());
  }

}
