/**
 * 分组前端逻辑
 *
 * Created by Watson W on 2016/5/6.
 */
charity.config(['$stateProvider', function ($stateProvider) {
  $stateProvider.state('account', {
    abstract: true,
    url: '/account',
    templateUrl: 'views/commons/commons.html'
  }).state('account.list', {
    url: '',
    controller: 'accountController',
    templateUrl: 'views/account/list.html'
  }).state('account.operation', {
    url: "/operation/:pkid",
    templateUrl: "views/account/operation.html",
    controller: 'accountOpController'
  });
}]).controller('accountController', ['$scope', '$http', '$state', function ($scope, $http, $state) {
  $scope.index.subTitle = "用户列表";
  $scope.account = {
    page: {
      total: 0,
      current: 1,
      pageSize: 10,
      changePage: function (i) {
        $scope.account.page.current = i;
        loadData();
      }
    },
    datas: []
  };

  $scope.toOperation = function (pkid) {
    $state.go("account.operation", {"pkid": pkid});
  };

  $scope.delete = function (pkid) {
    layer.confirm('确认删除用户？', {
      btn: ['删除', '取消'] //按钮
    }, function () {
      $http.delete('account/' + pkid, {cache: false}).then(function (r) {
        var d = r.data;
        layer.alert(d.msg);
        loadData();
      },function (r) {
        var d = r.data;
        layer.alert(d.msg);
        console.log(d.data);
      });
    });
  };

  function loadData() {
    $http.post('account', JSON.stringify({
      "page": $scope.account.page.current,
      "pageSize": $scope.account.page.pageSize
    }), {cache: false}).then(function (r) {
      var d = r.data;
      if (d.success) {
        $scope.account.page.current = d.data.pageNum;
        $scope.account.page.pageSize = d.data.pageSize;
        $scope.account.page.total = d.data.total;
        $scope.account.datas = d.data.data;
      } else {
        layer.alert(d.msg);
      }
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  }

  loadData();

}]).controller('accountOpController', ['$scope', '$http', '$state', '$stateParams', '$validation', function ($scope, $http, $state, $stateParams, $validation) {
  var pkid = $stateParams.pkid;
  $scope.operation = {
    data: undefined,
    permissions: [],
    groups: []
  };

  $http.get('account/combox', {cache: false}).then(function (r) {
    var d = r.data;
    if (d.success) {
      $scope.operation.permissions = d.data.permissions;
      $scope.operation.groups = d.data.groups;
    } else {
      layer.alert(d.msg);
    }
  },function (r) {
    var d = r.data;
    layer.alert(d.msg);
    console.log(d.data);
  });

  if (pkid) {
    $scope.index.subTitle = "修改用户";
    $http.get('account/' + pkid, {cache: false}).then(function (r) {
      var d = r.data;
      if (d.success) {
        $scope.operation.data = d.data;
      } else {
        layer.alert(d.msg);
      }
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  } else {
    $scope.index.subTitle = "新增用户";
  }

  $scope.save = function (form) {
    $validation.validate(form).success(function () {
      var method = "PUT", url = "account";
      if (pkid) {
        method = "POST";
        url += "/" + pkid;
      } else {
        if (!$scope.operation.data.passWord) {
          layer.alert('新增用户时必须填写密码!');
          return;
        }
      }

      $http({
        method: method,
        url: url,
        data: JSON.stringify($scope.operation.data)
      }).then(function (r) {
        var d = r.data;
        layer.alert(d.msg);
        $state.go("account.list");
      },function (r) {
        var d = r.data;
        layer.alert(d.msg);
        console.log(d.data);
      });
    },function () {
      layer.alert('请完整填写后在提交!');
    });
  };


  $scope.cancel = function () {
    $state.go("account.list");
  };

}]);
