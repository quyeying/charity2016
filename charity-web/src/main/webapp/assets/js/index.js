var charity = angular.module('charity', ['ui.router', 'ui.bootstrap', 'validation', 'validation.rule', 'qyy'])
  .factory('loginInterceptor', ['$q', '$injector', function ($q, $injector) {
    return {
      'request': function (config) {
        layer.load(0, {shade: 0.3});
        return config;
      },
      'requestError': function (rejection) {
        layer.closeAll('loading');
        return $q.reject(rejection);
      },
      'response': function (response) {
        layer.closeAll('loading');
        return response;
      },
      'responseError': function (response) {
        layer.closeAll('loading');
        var state = $injector.get('$state');
        switch (response.status) {
          case 401:
            state.go('login');
            break;
          case 403:
            // 无权限
            state.go('403');
            break;
          case 404:
            // 找不到
            state.go('404');
            break;
        }
        return $q.reject(response);
      }
    };
  }]).config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$validationProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider, $validationProvider) {
      $stateProvider.state("main", {
        url: "/",
        templateUrl: "views/main/main.html",
        controller: 'mainController'
      });
      $urlRouterProvider.otherwise('/');
      $httpProvider.interceptors.push('loginInterceptor');
      angular.extend($validationProvider, {
        validCallback: function (element) {
          $(element).parents('.form-group:first').removeClass('has-error');
        },
        invalidCallback: function (element) {
          $(element).parents('.form-group:first').addClass('has-error');
        }
      });
      $validationProvider.setErrorHTML(function (msg) {
        return "<label class=\"control-label has-error\">" + msg + "</label>";
      });
    }
  ]);
charity.controller('indexController', ['$scope', '$state', '$window', function ($scope, $state, $window) {

  $scope.index = {
    navbar: true,
    menus: [],
    account: undefined,
    subTitle: undefined
  };
  if ($window.sessionStorage["menus"]) {
    $scope.index.menus = JSON.parse($window.sessionStorage["menus"]);
  }
  if ($window.sessionStorage["account"]) {
    $scope.index.account = JSON.parse($window.sessionStorage["account"]);
  }

  $scope.go = function (state) {
    $state.go(state);
  };

  $scope.logout = function () {
    $window.sessionStorage.removeItem("menus");
    $window.sessionStorage.removeItem("account");
    window.location.href = "logout";
  }

}]);

