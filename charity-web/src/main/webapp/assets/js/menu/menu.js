/**
 * 分组前端逻辑
 *
 * Created by Watson W on 2016/5/6.
 */
charity.config(['$stateProvider', function ($stateProvider) {
  $stateProvider.state('menu', {
    abstract: true,
    url: '/menu',
    templateUrl: 'views/commons/commons.html'
  }).state('menu.list', {
    url: '',
    controller: 'menuController',
    templateUrl: 'views/menu/list.html'
  }).state('menu.operation', {
    url: "/operation/:pkid",
    templateUrl: "views/menu/operation.html",
    controller: 'menuOpController'
  });
}]).controller('menuController', ['$scope', '$http', '$state', function ($scope, $http, $state) {
  $scope.index.subTitle = "用户列表";
  $scope.menu = {
    page: {
      total: 0,
      current: 1,
      pageSize: 10,
      changePage: function (i) {
        $scope.menu.page.current = i;
        loadData();
      }
    },
    datas: []
  };

  $scope.toOperation = function (pkid) {
    $state.go("menu.operation", {"pkid": pkid});
  };

  $scope.delete = function (pkid) {
    layer.confirm('确认删除用户？', {
      btn: ['删除', '取消'] //按钮
    }, function () {
      $http.delete('menu/' + pkid, {cache: false}).then(function (r) {
        var d = r.data;
        layer.alert(d.msg);
        loadData();
      },function (r) {
        var d = r.data;
        layer.alert(d.msg);
        console.log(d.data);
      });
    });
  };

  function loadData() {
    $http.post('menu', JSON.stringify({
      "page": $scope.menu.page.current,
      "pageSize": $scope.menu.page.pageSize
    }), {cache: false}).then(function (r) {
      var d = r.data;
      if (d.success) {
        $scope.menu.page.current = d.data.pageNum;
        $scope.menu.page.pageSize = d.data.pageSize;
        $scope.menu.page.total = d.data.total;
        $scope.menu.datas = d.data.data;
      } else {
        layer.alert(d.msg);
      }
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  }

  loadData();

}]).controller('menuOpController', ['$scope', '$http', '$state', '$stateParams', '$validation', function ($scope, $http, $state, $stateParams, $validation) {
  var pkid = $stateParams.pkid;
  $scope.operation = {
    data: undefined
  };

  if (pkid) {
    $scope.index.subTitle = "修改菜单";
    $http.get('menu/' + pkid, {cache: false}).then(function (r) {
      var d = r.data;
      if (d.success) {
        $scope.operation.data = d.data;
      } else {
        layer.alert(d.msg);
      }
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  } else {
    $scope.index.subTitle = "新增菜单";
  }

  $scope.save = function (form) {
    $validation.validate(form).success(function () {
      var method = "PUT", url = "menu";
      if (pkid) {
        method = "POST";
        url += "/" + pkid;
      }

      $http({
        method: method,
        url: url,
        data: JSON.stringify($scope.operation.data)
      }).then(function (r) {
        var d = r.data;
        layer.alert(d.msg);
        $state.go("menu.list");
      },function (r) {
        var d = r.data;
        layer.alert(d.msg);
        console.log(d.data);
      });
    }).error(function () {
      layer.alert('请完整填写后在提交!');
    });
  };


  $scope.cancel = function () {
    $state.go("menu.list");
  };

}]);
