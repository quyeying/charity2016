angular.module('qyy', []).directive('qyyPager', function () {
  return {
    restrict: 'EA',
    replace: true,
    transclude: true,
    scope: {
      total: '=total',
      current: '=current',
      pageSize: '=pageSize',
      changePage: '=changePage'
    },
    template: '<nav><ul class="pagination">' +
    '<li ng-class="{\'disabled\':1 == current}"><a ng-click="change(current-1)"><span aria-hidden="true">&laquo;</span></a></li>' +
    '<li ng-class="{\'disabled\':1 == current}"><a ng-click="change(1)" >首</a></li>' +
    '<li ng-class="{\'active\': i == current}" ng-repeat="i in pages"><a ng-click="change(i)" ng-bind="i"></a></li>' +
    '<li ng-class="{\'disabled\':totalPage == current}"><a ng-click="change(totalPage)" >尾</a></li>' +
    '<li ng-class="{\'disabled\':totalPage == current}"><a ng-click="change(current+1)" ><span aria-hidden="true">&raquo;</span></a></li>' +
    '</ul></nav>',
    link: function (scope, element, attrs) {

      function init() {
        scope.totalPage = parseInt(Math.ceil(scope.total / scope.pageSize));
        var result = [];
        var start, end;
        if (scope.totalPage < 6) {
          start = 1;
          end = scope.totalPage
        } else {
          var middle = scope.current;
          if (scope.current < 3) {
            middle = 3;
          }

          if (scope.current + 2 > scope.totalPage) {
            middle = scope.totalPage - 2;
          }
          start = middle - 2;
          end = middle + 2;
        }

        for (var i = start; i <= end; i++) {
          result.push(i);
        }
        scope.pages = result;
      }

      init();

      scope.$watchGroup(['total', 'current', 'pageSize'], function () {
        init();
      });

      scope.change = function (i) {
        if (i < 1 || i > scope.totalPage || i == scope.current) {
          return;
        }
        scope.changePage(i);
      }
    }
  };
});
