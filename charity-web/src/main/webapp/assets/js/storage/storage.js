/**
 * 出入库管理
 *
 * Created by Watson W on 2016/5/18.
 */
charity.config(['$stateProvider', function ($stateProvider) {
  $stateProvider.state('storage', {
    abstract: true,
    url: '/storage',
    templateUrl: 'views/commons/commons.html'
  }).state('storage.opt', {
    url: '',
    controller: 'storageOptController',
    templateUrl: 'views/storage/opt.html'
  }).state('storage.settlement', {
    url: "/settlement",
    templateUrl: "views/storage/settlement.html",
    controller: 'storageSettlementController'
  });
}]).controller('storageOptController', ['$scope', '$http', '$state', function ($scope, $http, $state) {
  $scope.index.subTitle = "出入库管理";
  $scope.goods = {
    inp: undefined,
    load: undefined
  };

  $scope.inpNum = function () {
    $http.get('goods/' + $scope.goods.inp.num, {cache: false}).then(function (r) {
      var d = r.data;
      if(d.success){
        $scope.goods.load = d.data;
      }else{
        $scope.goods.load = undefined;
      }

    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });

  };
}]).controller('storageSettlementController', ['$scope', '$http', '$state', "$stateParams", "$validation", function ($scope, $http, $state, $stateParams, $validation) {
  $scope.index.subTitle = "订单结算";

}]);

