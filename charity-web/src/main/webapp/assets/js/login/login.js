/**
 * Created by ben on 2016/4/29/029.
 * 登陆
 */
charity.config(['$stateProvider', function ($stateProvider) {
  $stateProvider.state("login", {
    url: "/login",
    templateUrl: "views/login/login.html",
    controller: 'loginController'
  });
}]).controller('loginController', ['$scope', '$http', '$state', '$window', function ($scope, $http, $state, $window) {
  $scope.login = {};

  $scope.doLogin = function () {
    if (!$scope.login.userName) {
      layer.alert('请填写用户名', {
        skin: 'layui-layer-lan',
        closeBtn: 0,
        shadeClose: true
      });
      return;
    }
    if (!$scope.login.passWord) {
      layer.alert('请填写密码', {
        skin: 'layui-layer-lan',
        closeBtn: 0,
        shadeClose: true
      });
      return;
    }


    // jsonp方式登录 保证正确写入cookie
    $http.jsonp("login?callback=JSON_CALLBACK", {
      params: {
        "userName": $scope.login.userName,
        "passWord": $scope.login.passWord,
        "rememberMe": $scope.login.rememberMe
      },
      cache: false
    }).then(function (r) {
      var d = r.data;
      if (d.success) {
        $window.sessionStorage["account"] = JSON.stringify(d.data);
        $scope.index.account = d.data;
        $state.go('main');
      } else {
        layer.alert(d.msg, {
          skin: 'layui-layer-lan',
          closeBtn: 0,
          shadeClose: true
        });
      }
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  };

}]);
