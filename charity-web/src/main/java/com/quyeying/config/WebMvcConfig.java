package com.quyeying.config;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.quyeying.core.advice.JsonpAdvice;
import com.quyeying.core.controller.CustomExceptionResolve;
import com.quyeying.core.converter.DateConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;

/**
 * web mvc config spring-mvc.xml
 * <p>
 * Created by Watson W on 2016/4/9.
 */
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableWebMvc
@ComponentScan(basePackages = "com.quyeying.charity",
  useDefaultFilters = false,
  includeFilters = @ComponentScan.Filter({Controller.class, RestController.class, ControllerAdvice.class}))
public class WebMvcConfig extends WebMvcConfigurerAdapter {

  private static final Charset DEFAUIT_CHARSET = Charsets.UTF_8;

  @Override
  public void addFormatters(FormatterRegistry registry) {
    registry.addConverter(new DateConverter());
    super.addFormatters(registry);
  }

  @Override
  public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
    // 文本转换器
    StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
    stringHttpMessageConverter.setSupportedMediaTypes(Lists.newArrayList(
      new MediaType("text", "plain", DEFAUIT_CHARSET),
      new MediaType("text", "html", DEFAUIT_CHARSET)
    ));
    // json转换器
    MappingJackson2HttpMessageConverter jackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
    jackson2HttpMessageConverter.setSupportedMediaTypes(Lists.newArrayList(new MediaType("application", "json", DEFAUIT_CHARSET)));
    // 下载转换器
    converters.add(new ByteArrayHttpMessageConverter());
    converters.add(stringHttpMessageConverter);
    converters.add(jackson2HttpMessageConverter);
    super.extendMessageConverters(converters);
  }

  @Override
  public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
    // 异常处理
    CustomExceptionResolve customExceptionResolve = new CustomExceptionResolve();
    customExceptionResolve.addStatusCode("error/500", 500);
    exceptionResolvers.add(customExceptionResolve);
    super.configureHandlerExceptionResolvers(exceptionResolvers);
  }

  /**
   * spring validator
   *
   * @return LocalValidatorFactoryBean
   */
  @Bean
  public LocalValidatorFactoryBean validator() {
    LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
    Properties properties = new Properties();
    properties.setProperty("providerClass", "org.hibernate.validator.HibernateValidator");
    localValidatorFactoryBean.setValidationProperties(properties);
    return localValidatorFactoryBean;
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/assets/**").addResourceLocations("/assets/");
    registry.addResourceHandler("/views/**").addResourceLocations("/views/");
    super.addResourceHandlers(registry);
  }

  @Override
  public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
    configurer.enable();
  }

  /**
   * JsonpAdvice
   *
   * @return JsonpAdvice
   */
  @Bean
  public JsonpAdvice jsonpAdvice() {
    return new JsonpAdvice();
  }

}
