package com.quyeying.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.sql.SQLException;

/**
 * datasource Config
 * <p>
 * Created by Watson W on 2016/4/19.
 */
@Configuration
@PropertySource("classpath:/config.properties")
public class DatasourceConfig {

  private static final Logger LOGGER = LoggerFactory.getLogger(DatasourceConfig.class);

  @Autowired
  private Environment environment;

  @Bean(initMethod = "init", destroyMethod = "close")
  public DruidDataSource dataSource() {
    DruidDataSource dataSource = new DruidDataSource();
    dataSource.setUrl(environment.getProperty("jdbc.url"));
    dataSource.setUsername(environment.getProperty("jdbc.username"));
    dataSource.setPassword(environment.getProperty("jdbc.password"));
    // 初始化连接大小
    dataSource.setInitialSize(5);
    // 连接池最大使用连接数量
    dataSource.setMaxActive(50);
    // 连接池最小空闲
    dataSource.setMinIdle(5);
    // 获取连接最大等待时间
    dataSource.setMaxWait(60000L);
    dataSource.setValidationQuery("SELECT 1 FROM DUAL");
    dataSource.setTestOnBorrow(false);
    dataSource.setTestOnReturn(false);
    dataSource.setTestWhileIdle(true);
    // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
    dataSource.setTimeBetweenEvictionRunsMillis(60000L);
    // 配置一个连接在池中最小生存的时间，单位是毫秒
    dataSource.setMinEvictableIdleTimeMillis(25200000L);
    // 打开removeAbandoned功能
    dataSource.setRemoveAbandoned(false);
    // 1800秒，也就是30分钟
    dataSource.setRemoveAbandonedTimeout(60);
    // 关闭abanded连接时输出错误日志
    dataSource.setLogAbandoned(true);
    try {
      // 监控数据库
      dataSource.setFilters("mergeStat,slf4j");
    } catch (SQLException ignore) {
      LOGGER.error("初始化dataSource失败:{}", ignore.getMessage());
    }
    return dataSource;
  }


}
