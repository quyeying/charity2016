package com.quyeying.config;

import com.github.pagehelper.PageHelper;
import com.quyeying.core.mybatis.CameHumpInterceptor;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * mybatis Config
 * <p>
 * Created by Watson W on 2016/4/19.
 */
@Configuration
@Import({DatasourceConfig.class})
@EnableTransactionManagement(proxyTargetClass = true)
public class MyBatisConfig implements TransactionManagementConfigurer {

  private static final Logger LOGGER = LoggerFactory.getLogger(MyBatisConfig.class);

  @Autowired
  DataSource dataSource;

  @Bean
  @Override
  public PlatformTransactionManager annotationDrivenTransactionManager() {
    return new DataSourceTransactionManager(dataSource);
  }

  @Bean(name = "sqlSessionFactory")
  public SqlSessionFactory sqlSessionFactoryBean() {

    SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
    bean.setDataSource(dataSource);
    bean.setTypeAliasesPackage("com.quyeying.db.model");

    //分页插件
    PageHelper pageHelper = new PageHelper();
    Properties properties = new Properties();
    properties.setProperty("dialect", "mysql");
    properties.setProperty("pageSizeZero", "false");
    properties.setProperty("reasonable", "false");
    properties.setProperty("supportMethodsArguments", "false");
    properties.setProperty("returnPageInfo", "none");
    pageHelper.setProperties(properties);

    //添加插件
    bean.setPlugins(new Interceptor[]{pageHelper, new CameHumpInterceptor()});

    //添加XML目录
    ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
    try {
      bean.setMapperLocations(resolver.getResources("classpath:mapper/*.xml"));
      SqlSessionFactory result = bean.getObject();
      result.getConfiguration().setMapUnderscoreToCamelCase(true);
      return result;
    } catch (Exception e) {
      LOGGER.error("创建sqlSessionFactoryBean失败:{}", e.getMessage());
      throw new RuntimeException(e);
    }
  }

  @Bean
  public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
    return new SqlSessionTemplate(sqlSessionFactory);
  }
}
