package com.quyeying.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.quyeying.core.listener.SessionListener;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.EnumSet;

/**
 * 初始化 servlet web.xml
 * <p>
 * Created by Watson W on 2016/4/9.
 */
public class WebInitialConfiguration extends AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class<?>[]{ShiroConfig.class};
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    // 设置spring主配置文件
    return new Class<?>[]{WebMvcConfig.class};
  }

  @Override
  protected String[] getServletMappings() {
    // Servlet Mapping
    return new String[]{"/"};
  }

  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {
    // 设置session超时时间
    servletContext.setInitParameter("sessionTimeout", "600");
    servletContext.addListener(SessionListener.class);

    // encoding Filter
    FilterRegistration.Dynamic encodingFilter = servletContext.addFilter("encodingFilter", new CharacterEncodingFilter());
    encodingFilter.setInitParameter("encoding", "UTF-8");
    encodingFilter.setInitParameter("forceEncoding", "true");
    encodingFilter.addMappingForUrlPatterns(null, false, "/*");

    // shiro Filter
    FilterRegistration.Dynamic shiroFilter = servletContext.addFilter("shiroFilter", new DelegatingFilterProxy());
    shiroFilter.setInitParameter("targetFilterLifecycle", "true");
    shiroFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD), false, "/*");
    // druid datasource state monitor
    servletContext.addServlet("druid", new StatViewServlet()).addMapping("/druid/*");
    super.onStartup(servletContext);
  }

}
