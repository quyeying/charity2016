package com.quyeying.config;

import com.alibaba.druid.support.spring.stat.DruidStatInterceptor;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcutAdvisor;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.spring.mapper.MapperScannerConfigurer;

/**
 * mapper scanner Config
 * <p>
 * Created by Watson W on 2016/4/19.
 */
@Configuration
@Import({MyBatisConfig.class})
@ComponentScan(basePackages = "com.quyeying.charity",
  excludeFilters = @ComponentScan.Filter({Controller.class, RestController.class, ControllerAdvice.class}))
public class MyBatisMapperScannerConfig {
  @Bean
  public MapperScannerConfigurer mapperScannerConfigurer() {
    MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
    mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
    mapperScannerConfigurer.setBasePackage("com.quyeying.db.mapper");
    return mapperScannerConfigurer;
  }

  @Bean
  @Lazy(false)
  public DruidStatInterceptor druidStatInterceptor() {
    return new DruidStatInterceptor();
  }

  @Bean
  @Lazy(false)
  public Advisor druidStatAdvisor(DruidStatInterceptor druidStatInterceptor) {
    AspectJExpressionPointcutAdvisor advisor = new AspectJExpressionPointcutAdvisor();
    advisor.setAdvice(druidStatInterceptor);
    advisor.setExpression("execution(* com.quyeying.charity..service.*.*(..))");
    return advisor;
  }
}
