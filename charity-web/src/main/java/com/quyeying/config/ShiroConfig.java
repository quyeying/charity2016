package com.quyeying.config;

import com.quyeying.charity.user.service.LoginService;
import com.quyeying.core.security.RememberMetFilter;
import com.quyeying.core.security.RolesFilter;
import com.quyeying.core.security.ShiroDataBaseRealm;
import com.quyeying.core.security.ShiroMetaSource;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.mgt.RememberMeManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.*;

import javax.servlet.Filter;
import java.util.HashMap;
import java.util.Map;

/**
 * Shiro Config
 * <p>
 * Created by Watson W on 2016/4/19.
 */
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@Import({MyBatisMapperScannerConfig.class})
public class ShiroConfig {

  @Bean
  public RolesFilter rolesFilter() {
    return new RolesFilter();
  }

  @Bean
  public RememberMetFilter rememberMetFilter() {
    return new RememberMetFilter();
  }

  @Bean
  public ShiroDataBaseRealm shiroRealm(LoginService loginService) {
    return new ShiroDataBaseRealm(loginService);
  }

  @Bean
  public EhCacheManager ehCacheManager() {
    EhCacheManager bean = new EhCacheManager();
    bean.setCacheManagerConfigFile("classpath:ehcache-shiro.xml");
    return bean;
  }

  @Bean
  public RememberMeManager rememberMeManager() {
    CookieRememberMeManager result = new CookieRememberMeManager();
    Cookie cookie = new SimpleCookie("rememberMe");
    cookie.setHttpOnly(true);
    cookie.setMaxAge(-1);
    cookie.setName("C_ID");
    result.setCookie(cookie);
    result.setCipherKey("jsSt$vFt$WHf1e^md@QKyLy4#rI!Q8O9".getBytes());
    return result;
  }

  @Bean
  public DefaultWebSecurityManager webSecurityManager(
    ShiroDataBaseRealm shiroRealm,
    EhCacheManager ehCacheManager,
    RememberMeManager rememberMeManager
  ) {
    DefaultWebSecurityManager bean = new DefaultWebSecurityManager();
    bean.setRealm(shiroRealm);
    bean.setCacheManager(ehCacheManager);
    bean.setRememberMeManager(rememberMeManager);
    return bean;
  }

  @Bean
  public ShiroMetaSource shiroMetaSource(LoginService loginService) {
    return new ShiroMetaSource(loginService, "/=anon\n" +
      "/favicon.ico=anon\n" +
      "/index.html=anon\n" +
      "/assets/**=anon\n" +
      "/views/**=anon\n" +
      "/login=anon\n" +
      "/401=anon\n" +
      "/403=anon\n" +
      "/logout=user,setSession",
      "/**=user,setSession");
  }

  @Bean(name = "shiroFilter")
  public AbstractShiroFilter getShiroFilter(
    DefaultWebSecurityManager webSecurityManager,
    ShiroMetaSource shiroMetaSource,
    RolesFilter rolesFilter,
    RememberMetFilter rememberMetFilter
  ) throws Exception {

    ShiroFilterFactoryBean factoryBean = new ShiroFilterFactoryBean();
    factoryBean.setSecurityManager(webSecurityManager);
    factoryBean.setLoginUrl("/401");
    factoryBean.setUnauthorizedUrl("/403");
    factoryBean.setFilterChainDefinitionMap(shiroMetaSource.getObject());
    Map<String, Filter> filters = new HashMap<>();
    filters.put("roleOr", rolesFilter);
    filters.put("setSession", rememberMetFilter);
    factoryBean.setFilters(filters);

    return (AbstractShiroFilter) factoryBean.getObject();
  }

  @Bean
  public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
    return new LifecycleBeanPostProcessor();
  }

  @Bean
  @DependsOn(value = "lifecycleBeanPostProcessor")
  public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
    return new DefaultAdvisorAutoProxyCreator();
  }
}
