package com.quyeying.charity.goods.controller;

import com.quyeying.charity.goods.dto.GoodsDto;
import com.quyeying.charity.goods.service.GoodsService;
import com.quyeying.core.dto.JsonResult;
import com.quyeying.core.security.Account;
import com.quyeying.core.security.SessionUtils;
import com.quyeying.db.mapper.TGoodsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 商品
 * <p>
 * Created by Watson W on 2016/4/27.
 */
@RestController
@RequestMapping("/goods")
public class GoodsCtrl {

  private static final Logger LOGGER = LoggerFactory.getLogger(GoodsCtrl.class);

  @Resource
  private GoodsService goodsService;
  @Resource
  private TGoodsMapper goodsMapper;

  @RequestMapping(value = "/{goodsNum}",method = RequestMethod.GET)
  @ResponseBody
  public JsonResult queryGoods(@PathVariable("goodsNum") Integer goodsNum){
    Account user = SessionUtils.getCurrentUser();
    String pkid = user.getUserGroup()+String.format("%03d",goodsNum);
    GoodsDto goods = goodsMapper.selectGoodsInfoByPkid(pkid);
    if(null == goods){
      return JsonResult.failure("未录入");
    }else{
      return JsonResult.success(goods);
    }
  }

}
