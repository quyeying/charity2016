package com.quyeying.charity.goods.dto;

/**
 * Created by ben on 2016/8/8.
 */
public class GoodsDto {
  private String pkid;
  private String personName;
  private String personPhone;
  private String goodsName;
  private Integer goodsCount;
  private Integer goodsPrice;
  private String goodsReturn;
  private String remark;
  private Integer saleCount;

  public String getPkid() {
    return pkid;
  }

  public void setPkid(String pkid) {
    this.pkid = pkid;
  }

  public String getPersonName() {
    return personName;
  }

  public void setPersonName(String personName) {
    this.personName = personName;
  }

  public String getPersonPhone() {
    return personPhone;
  }

  public void setPersonPhone(String personPhone) {
    this.personPhone = personPhone;
  }

  public String getGoodsName() {
    return goodsName;
  }

  public void setGoodsName(String goodsName) {
    this.goodsName = goodsName;
  }

  public Integer getGoodsCount() {
    return goodsCount;
  }

  public void setGoodsCount(Integer goodsCount) {
    this.goodsCount = goodsCount;
  }

  public Integer getGoodsPrice() {
    return goodsPrice;
  }

  public void setGoodsPrice(Integer goodsPrice) {
    this.goodsPrice = goodsPrice;
  }

  public String getGoodsReturn() {
    return goodsReturn;
  }

  public void setGoodsReturn(String goodsReturn) {
    this.goodsReturn = goodsReturn;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Integer getSaleCount() {
    return saleCount;
  }

  public void setSaleCount(Integer saleCount) {
    this.saleCount = saleCount;
  }
}
