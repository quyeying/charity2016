package com.quyeying.charity.user.controller;

import com.quyeying.db.model.TMenus;
import com.quyeying.charity.user.dto.LoginDto;
import com.quyeying.charity.user.service.LoginService;
import com.quyeying.core.advice.EnableJsonpAdvice;
import com.quyeying.core.dto.JsonResult;
import com.quyeying.core.security.Account;
import com.quyeying.core.security.SessionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 登录 controller
 * <p>
 * Created by Watson W on 2016/4/13.
 */
@EnableJsonpAdvice
@RestController
@RequestMapping
public class LoginCtrl {

  private static final Logger LOGGER = LoggerFactory.getLogger(LoginCtrl.class);

  @Resource
  private LoginService loginService;

  /**
   * 登录
   *
   * @param dto 登录参数
   * @return 登录结果
   */
  @RequestMapping(value = "/login", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public JsonResult login(LoginDto dto) {
    Subject subject = SecurityUtils.getSubject();
    UsernamePasswordToken token = new UsernamePasswordToken(dto.getUserName(), dto.getPassWord(), dto.isRememberMe());
    try {
      subject.login(token);
      Account account = SessionUtils.getCurrentUser();
      return JsonResult.success(account, "登录成功!");
    } catch (UnknownAccountException e) {
      LOGGER.info("登录失败,无此用户", e);
      SessionUtils.removeCurrentUser();
      return JsonResult.failure("登录失败,无此用户!");
    } catch (IncorrectCredentialsException e) {
      LOGGER.info("登录失败,密码错误:", e);
      SessionUtils.removeCurrentUser();
      return JsonResult.failure("登录失败,密码错误!");
    } catch (AuthenticationException e) {
      LOGGER.info("登录失败:", e);
      SessionUtils.removeCurrentUser();
      return JsonResult.failure("登录失败!");
    }
  }

  /**
   * 获取当前用户菜单
   *
   * @return 菜单
   */
  @RequestMapping(value = "/menus", method = RequestMethod.GET)
  public JsonResult menus() {
    Account account = SessionUtils.getCurrentUser();
    List<TMenus> menus = loginService.userMenus(account.getPkid());
    return JsonResult.success(menus, "查询成功");
  }

  /**
   * 获取当前用户
   *
   * @return 菜单
   */
  @RequestMapping(value = "/who", method = RequestMethod.GET)
  public JsonResult who() {
    Account account = SessionUtils.getCurrentUser();
    return JsonResult.success(account, "查询成功");
  }
}
