package com.quyeying.charity.user.controller;

import com.quyeying.core.security.SessionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 退出登录
 * Created by ben on 2016/5/3/003.
 */
@Controller
@RequestMapping
public class LogoutCtrl {

  private static final Logger LOGGER = LoggerFactory.getLogger(LoginCtrl.class);

  /**
   * 登出
   *
   * @return 登出结果
   */
  @RequestMapping(value = "/logout", method = RequestMethod.GET)
  public String logout() {
    try {
      Subject subject = SecurityUtils.getSubject();
      subject.logout();
      SessionUtils.removeCurrentUser();
    } catch (Exception e) {
      LOGGER.info("用户登出时发生错误", e);
    }
    return "redirect:/";
  }

  @RequestMapping(value = "/403")
  @ResponseStatus(HttpStatus.FORBIDDEN)
  public void forbidden() {
    // 未登陆需要登陆
  }

  @RequestMapping(value = "/401")
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  public void unauthorized() {
    // 无权访问
  }

}
