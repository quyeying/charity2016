package com.quyeying.charity.user.service;

import com.quyeying.db.model.TMenus;
import com.quyeying.core.security.IShiroUserService;

import java.util.List;

/**
 * LoginService
 * <p>
 * Created by Watson W on 2016/4/20.
 */
public interface LoginService extends IShiroUserService {

  /**
   * 根据账户id获取其菜单
   *
   * @param accountId 账户id
   * @return 菜单
   */
  List<TMenus> userMenus(Long accountId);
}
