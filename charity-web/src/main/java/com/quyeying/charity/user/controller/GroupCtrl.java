package com.quyeying.charity.user.controller;

import com.quyeying.charity.user.dto.GroupQueryDto;
import com.quyeying.charity.user.service.GroupService;
import com.quyeying.core.dto.PageWrapper;
import com.quyeying.db.model.TGroup;
import com.quyeying.core.dto.JsonResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 分组 controller
 * <p>
 * Created by Watson W on 2016/4/27.
 */
@RestController
@RequestMapping("/group")
public class GroupCtrl {

  private static final Logger LOGGER = LoggerFactory.getLogger(GroupCtrl.class);

  @Resource
  private GroupService groupService;

  /**
   * 查询分组
   *
   * @param pkid 主键
   * @return JsonResult
   */
  @RequestMapping(value = "/{pkid}", method = RequestMethod.GET)
  public JsonResult group(@PathVariable String pkid) {
    return JsonResult.success(groupService.group(pkid));
  }

  /**
   * 根据分页查询分组信息集合
   *
   * @param dto    查询参数
   * @param errors 校验信息
   * @return JsonResult
   */
  @RequestMapping(method = RequestMethod.POST)
  public JsonResult groups(@Valid @RequestBody GroupQueryDto dto, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用查询分组信息集合接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用查询分组信息集合接口参数错误", errors.getAllErrors());
    }
    return JsonResult.success(PageWrapper.fromPage(groupService.groups(dto)));
  }

  /**
   * 添加分组信息
   *
   * @param group  分组实体
   * @param errors 校验信息
   * @return JsonResult
   */
  @RequestMapping(method = RequestMethod.PUT)
  public JsonResult add(@Valid @RequestBody TGroup group, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用添加分组信息接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用添加分组信息接口参数错误", errors.getAllErrors());
    }
    groupService.add(group);
    return JsonResult.successMsg("添加分组信息成功!");
  }

  /**
   * 修改分组信息
   *
   * @param pkid   主键
   * @param group  分组实体
   * @param errors 校验信息
   * @return JsonResult
   */
  @RequestMapping(value = "/{pkid}", method = RequestMethod.POST)
  public JsonResult editByPrimary(@Valid @PathVariable String pkid, @Valid @RequestBody TGroup group, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用修改分组信息接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用修改分组信息接口参数错误", errors.getAllErrors());
    }
    group.setPkid(pkid);
    groupService.editByPrimary(group);
    return JsonResult.successMsg("修改分组信息成功!");
  }

  /**
   * 修改分组信息
   *
   * @param pkid 主键
   * @return JsonResult
   */
  @RequestMapping(value = "/{pkid}", method = RequestMethod.DELETE)
  public JsonResult delete(@Valid @PathVariable String pkid) {
    groupService.delete(pkid);
    return JsonResult.successMsg("删除分组成功!");
  }
}
