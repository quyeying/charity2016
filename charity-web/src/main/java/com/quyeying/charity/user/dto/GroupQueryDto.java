package com.quyeying.charity.user.dto;

import com.quyeying.core.dto.PageParams;

/**
 * 分组查询对象
 * <p>
 * Created by Watson W on 2016/4/27.
 */
public class GroupQueryDto extends PageParams {

  private static final long serialVersionUID = -1017377531633979029L;

}
