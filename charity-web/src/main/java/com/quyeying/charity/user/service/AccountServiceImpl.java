package com.quyeying.charity.user.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.quyeying.charity.user.dto.AccountListDto;
import com.quyeying.charity.user.dto.AccountQueryDto;
import com.quyeying.core.dto.Combox;
import com.quyeying.db.mapper.TAccountMapper;
import com.quyeying.db.mapper.TGroupMapper;
import com.quyeying.db.mapper.TPermissionsMapper;
import com.quyeying.db.model.TAccount;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 账户service
 * <p>
 * Created by Watson W on 2016/4/23.
 */
@Service
public class AccountServiceImpl implements AccountService {

  @Resource
  private TAccountMapper mapper;
  @Resource
  private TGroupMapper groupMapper;
  @Resource
  private TPermissionsMapper permissionsMapper;

  @Override
  public TAccount account(Long uid) {
    return mapper.selectByPrimaryKey(uid);
  }

  @Override
  public Page<AccountListDto> accounts(AccountQueryDto dto) {
    PageHelper.startPage(dto.getPage(), dto.getPageSize());
    return (Page<AccountListDto>) mapper.findForList(dto);
  }

  @Override
  public void add(TAccount account) {
    mapper.insert(account);
  }

  @Override
  public void editByPrimary(TAccount account) {
    mapper.updateByPrimaryKeySelective(account);
  }

  @Override
  public List<Combox> groups() {
    return groupMapper.findCombox();
  }

  @Override
  public List<Combox> permissions() {
    return permissionsMapper.findCombox();
  }

  @Override
  public void deleteAccount(Long uid) {
    mapper.deleteByPrimaryKey(uid);
  }

}
