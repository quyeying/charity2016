package com.quyeying.charity.user.service;

import com.quyeying.db.mapper.TAccountMapper;
import com.quyeying.db.mapper.TMenusMapper;
import com.quyeying.db.model.TAccount;
import com.quyeying.db.model.TMenus;
import com.quyeying.core.security.Account;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * LoginServiceImpl
 * <p>
 * Created by Watson W on 2016/4/20.
 */
@Service("loginService")
public class LoginServiceImpl implements LoginService {

  @Resource
  private TMenusMapper menusMapper;

  @Resource
  private TAccountMapper accountMapper;

  @Override
  public List<TMenus> findAllPermission() {
    return menusMapper.findAll();
  }

  @Override
  public List<String> findPermissionsByAccount(Long pkid) {
    return menusMapper.findPermissionsByAccount(pkid);
  }

  @Override
  public Account findAccountByName(String name) {
    Example example = new Example(TAccount.class);
    example.createCriteria().andEqualTo("userName", name);
    List<TAccount> list = accountMapper.selectByExample(example);
    if (list.isEmpty()) {
      return null;
    }
    return list.get(0);
  }

  @Override
  public List<TMenus> userMenus(Long accountId) {
    return menusMapper.findUserMenusById(accountId);
  }
}
