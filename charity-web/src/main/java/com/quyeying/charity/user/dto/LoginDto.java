package com.quyeying.charity.user.dto;

import java.io.Serializable;

/**
 * 登录数据传输对象
 * <p>
 * Created by Watson W on 2016/4/13.
 */
public class LoginDto implements Serializable {

  private static final long serialVersionUID = -6503412011527780970L;

  private String userName;

  private String passWord;

  private boolean rememberMe;

  @Override
  public String toString() {
    return "LoginDto{" +
      "userName='" + userName + '\'' +
      ", passWord='" + passWord + '\'' +
      ", rememberMe=" + rememberMe +
      '}';
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassWord() {
    return passWord;
  }

  public void setPassWord(String passWord) {
    this.passWord = passWord;
  }

  public boolean isRememberMe() {
    return rememberMe;
  }

  public void setRememberMe(boolean rememberMe) {
    this.rememberMe = rememberMe;
  }
}
