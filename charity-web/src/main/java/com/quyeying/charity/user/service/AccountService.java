package com.quyeying.charity.user.service;

import com.github.pagehelper.Page;
import com.quyeying.charity.user.dto.AccountListDto;
import com.quyeying.charity.user.dto.AccountQueryDto;
import com.quyeying.core.dto.Combox;
import com.quyeying.db.model.TAccount;

import java.util.List;

/**
 * 账户service
 * <p>
 * Created by Watson W on 2016/4/23.
 */
public interface AccountService {

  /**
   * 查询账户
   *
   * @param uid 账户标识
   * @return 账户信息
   */
  TAccount account(Long uid);

  /**
   * 查询所有账户
   *
   * @param dto 查询参数
   * @return 账户集合
   */
  Page<AccountListDto> accounts(AccountQueryDto dto);

  /**
   * 添加账户
   *
   * @param account 账户信息
   */
  void add(TAccount account);

  /**
   * 修改账户
   *
   * @param account 账户信息
   */
  void editByPrimary(TAccount account);

  /**
   * 所有用户组
   *
   * @return 用户组
   */
  List<Combox> groups();

  /**
   * 所有权限
   *
   * @return 权限
   */
  List<Combox> permissions();

  /**
   * 根据主键删除 account
   *
   * @param uid 主键
   */
  void deleteAccount(Long uid);
}
