package com.quyeying.charity.user.dto;

import com.quyeying.core.dto.PageParams;

/**
 * 查询对象
 * <p>
 * Created by Watson W on 2016/4/23.
 */
public class AccountQueryDto extends PageParams {

  private static final long serialVersionUID = 4304370991836921467L;

}
