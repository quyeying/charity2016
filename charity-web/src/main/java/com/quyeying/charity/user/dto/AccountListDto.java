package com.quyeying.charity.user.dto;

/**
 * Created by bysun on 2016/5/15.
 */
public class AccountListDto {
  private Long pkid;
  private String permission;
  private String userName;
  private String nikeName;
  private String group;

  public Long getPkid() {
    return pkid;
  }

  public void setPkid(Long pkid) {
    this.pkid = pkid;
  }

  public String getPermission() {
    return permission;
  }

  public void setPermission(String permission) {
    this.permission = permission;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getNikeName() {
    return nikeName;
  }

  public void setNikeName(String nikeName) {
    this.nikeName = nikeName;
  }

  public String getGroup() {
    return group;
  }

  public void setGroup(String group) {
    this.group = group;
  }
}
