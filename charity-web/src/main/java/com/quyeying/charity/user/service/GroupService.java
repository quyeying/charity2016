package com.quyeying.charity.user.service;

import com.github.pagehelper.Page;
import com.quyeying.db.model.TGroup;
import com.quyeying.charity.user.dto.GroupQueryDto;

/**
 * 分组service
 * <p>
 * Created by Watson W on 2016/4/27.
 */
public interface GroupService {

  /**
   * 根据主键查询分组信息
   *
   * @param pkid 主键
   * @return 分组信息
   */
  TGroup group(String pkid);

  /**
   * 根据分页查询分组信息集合
   *
   * @param dto 查询参数
   * @return 分组信息集合
   */
  Page<TGroup> groups(GroupQueryDto dto);

  /**
   * 添加分组信息
   *
   * @param group 分组信息
   */
  void add(TGroup group);

  /**
   * 修改分组信息
   *
   * @param group 分组信息
   */
  void editByPrimary(TGroup group);

  /**
   * 根据主键删除
   * @param pkid 主键
     */
  void delete(String pkid);
}
