package com.quyeying.charity.user.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.quyeying.db.mapper.TGroupMapper;
import com.quyeying.db.model.TGroup;
import com.quyeying.charity.user.dto.GroupQueryDto;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 分组service
 * <p>
 * Created by Watson W on 2016/4/27.
 */
@Service
public class GroupServiceImpl implements GroupService {

  @Resource
  private TGroupMapper mapper;

  @Override
  public TGroup group(String pkid) {
    return mapper.selectByPrimaryKey(pkid);
  }

  @Override
  public Page<TGroup> groups(GroupQueryDto dto) {
    PageHelper.startPage(dto.getPage(), dto.getPageSize());
    return (Page<TGroup>) mapper.selectAll();
  }

  @Override
  public void add(TGroup group) {
    mapper.insert(group);
  }

  @Override
  public void editByPrimary(TGroup group) {
    mapper.updateByPrimaryKey(group);
  }

  @Override
  public void delete(String pkid) {
    mapper.deleteByPrimaryKey(pkid);
  }

}
