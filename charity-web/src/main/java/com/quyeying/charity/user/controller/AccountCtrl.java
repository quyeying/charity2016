package com.quyeying.charity.user.controller;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.quyeying.charity.user.dto.AccountQueryDto;
import com.quyeying.charity.user.service.AccountService;
import com.quyeying.core.dto.JsonResult;
import com.quyeying.core.dto.PageWrapper;
import com.quyeying.db.model.TAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 账户 controller
 * <p>
 * Created by Watson W on 2016/4/13.
 */
@RestController
@RequestMapping("/account")
public class AccountCtrl {

  private static final Logger LOGGER = LoggerFactory.getLogger(AccountCtrl.class);

  @Resource
  private AccountService accountService;

  /**
   * 查询账户
   *
   * @param uid    账户标识
   * @return JsonResult
   */
  @RequestMapping(value = "/{uid}", method = RequestMethod.GET)
  public JsonResult account(@PathVariable Long uid) {
    return JsonResult.success(accountService.account(uid));
  }

  /**
   * 查询下拉
   *
   * @return JsonResult
   */
  @RequestMapping(value = "/combox", method = RequestMethod.GET)
  public JsonResult combox() {
    return JsonResult.success(ImmutableMap.of("groups",accountService.groups(),"permissions",accountService.permissions()));
  }

  /**
   * 查询所有用户
   *
   * @param dto    查询参数
   * @param errors 校验信息
   * @return JsonResult
   */
  @RequestMapping(method = RequestMethod.POST)
  public JsonResult accounts(@Valid @RequestBody AccountQueryDto dto, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用查询所有用户接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用查询所有用户接口参数错误", errors.getAllErrors());
    }
    return JsonResult.success(PageWrapper.fromPage(accountService.accounts(dto)));
  }

  /**
   * 添加账户
   *
   * @param account 账户实体
   * @param errors  校验信息
   * @return JsonResult
   */
  @RequestMapping( method = RequestMethod.PUT)
  public JsonResult add(@Valid @RequestBody TAccount account, Errors errors) {
    if (errors.hasErrors() || Strings.isNullOrEmpty(account.getPassWord())) {
      LOGGER.debug("调用添加用户接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用添加用户接口参数错误", errors.getAllErrors());
    }
    accountService.add(account);
    return JsonResult.successMsg("账号添加成功!");
  }

  /**
   * 修改账户
   *
   * @param uid     账户标识
   * @param account 账户实体
   * @param errors  校验信息
   * @return JsonResult
   */
  @RequestMapping(value = "/{uid}", method = RequestMethod.POST)
  public JsonResult editByPrimary(@PathVariable Long uid, @Valid @RequestBody TAccount account, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用修改账户接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用修改账户接口参数错误", errors.getAllErrors());
    }
    account.setPkid(uid);
    accountService.editByPrimary(account);
    return JsonResult.successMsg("账号修改成功!");
  }

  @RequestMapping(value = "/{uid}", method = RequestMethod.DELETE)
  public JsonResult delete(@PathVariable Long uid) {
    accountService.deleteAccount(uid);
    return JsonResult.successMsg("账号修改成功!");
  }

}
