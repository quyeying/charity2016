package com.quyeying.charity.permissions.dto;


import com.quyeying.db.model.TPermissions;

import java.util.List;

/**
 *
 *
 * Created by bysun on 2016/5/16.
 */
public class PermissionDto extends TPermissions {

  private static final long serialVersionUID = -9036342942304223438L;

  private List<Long> menuIds;

  public List<Long> getMenuIds() {
    return menuIds;
  }

  public void setMenuIds(List<Long> menuIds) {
    this.menuIds = menuIds;
  }

}
