package com.quyeying.charity.permissions.dto;

import com.quyeying.core.dto.PageParams;

/**
 * 权限查询对象
 * <p>
 * Created by Watson W on 2016/4/27.
 */
public class PermissionsQueryDto extends PageParams {

  private static final long serialVersionUID = -3825229108641140814L;

}
