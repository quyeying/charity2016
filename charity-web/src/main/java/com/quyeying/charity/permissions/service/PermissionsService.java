package com.quyeying.charity.permissions.service;

import com.github.pagehelper.Page;
import com.quyeying.charity.permissions.dto.PermissionDto;
import com.quyeying.charity.permissions.dto.PermissionsQueryDto;
import com.quyeying.db.model.TMenus;
import com.quyeying.db.model.TPermissions;

import java.util.List;

/**
 * 权限 service
 * <p>
 * Created by Watson W on 2016/4/27.
 */
public interface PermissionsService {

  /**
   * 根据主键查询权限信息
   *
   * @param pkid 主键
   * @return 权限信息
   */
  TPermissions permission(Long pkid);

  /**
   * 查询权限分页信息
   *
   * @param dto 查询参数
   * @return 权限集合
   */
  Page<TPermissions> permissions(PermissionsQueryDto dto);

  /**
   * 添加权限信息
   *
   * @param permissionDto 权限数据传输对象
   */
  void add(PermissionDto permissionDto);

  /**
   * 修改权限信息
   *
   * @param permissionDto 权限传输对象
   */
  void editByPrimary(PermissionDto permissionDto);

  /**
   * 获取所有菜单
   *
   * @return List<TMenus> 菜单集合
   */
  List<TMenus> menus();

  /**
   * 删除权限
   *
   * @param pkid 主键
   */
  void delete(Long pkid);

}
