package com.quyeying.charity.permissions.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.quyeying.charity.permissions.dto.PermissionDto;
import com.quyeying.charity.permissions.dto.PermissionsQueryDto;
import com.quyeying.db.mapper.TMenusMapper;
import com.quyeying.db.mapper.TPermissionsMapper;
import com.quyeying.db.mapper.TPermissionsMenuMapper;
import com.quyeying.db.model.TMenus;
import com.quyeying.db.model.TPermissions;
import com.quyeying.db.model.TPermissionsMenu;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 权限 service
 * <p>
 * Created by Watson W on 2016/4/27.
 */
@Service
public class PermissionsServiceImpl implements PermissionsService {

  @Resource
  private TPermissionsMapper mapper;

  @Resource
  private TMenusMapper menusMapper;

  @Resource
  private TPermissionsMenuMapper permissionsMenuMapper;

  @Override
  public TPermissions permission(Long pkid) {
    return mapper.selectByPrimaryKey(pkid);
  }

  @Override
  public Page<TPermissions> permissions(PermissionsQueryDto dto) {
    PageHelper.startPage(dto.getPage(), dto.getPageSize());
    return (Page<TPermissions>) mapper.selectAll();
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public void add(PermissionDto permissionDto) {
    TPermissions permissions = new PermissionDto();
    permissions.setPermissionName(permissionDto.getPermissionName());
    mapper.insert(permissions);

    saveHandle(permissionDto, permissions);
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public void editByPrimary(PermissionDto permissionDto) {
    mapper.updateByPrimaryKey(permissionDto);
    permissionsMenuMapper.deleteAllPrimaryKey(permissionDto.getPkid());
    saveHandle(permissionDto, permissionDto);
  }

  @Override
  public List<TMenus> menus() {
    return menusMapper.selectAll();
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public void delete(Long pkid) {
    mapper.deleteByPrimaryKey(pkid);
    permissionsMenuMapper.deleteAllPrimaryKey(pkid);
  }

  /**
   * 保存权限菜单方法
   *
   * @param permissionDto 权限菜单数据传输对象
   * @param permissions   实体
   */
  private void saveHandle(PermissionDto permissionDto, TPermissions permissions) {
    List<Long> menuIds = permissionDto.getMenuIds();
    for (Long menuId : menuIds) {
      TPermissionsMenu permissionsMenu = new TPermissionsMenu();
      permissionsMenu.setPermissionsId(permissions.getPkid());
      permissionsMenu.setMenuId(menuId);
      permissionsMenuMapper.insert(permissionsMenu);
    }
  }

}
