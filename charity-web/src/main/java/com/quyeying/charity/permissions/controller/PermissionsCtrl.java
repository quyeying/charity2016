package com.quyeying.charity.permissions.controller;

import com.quyeying.charity.permissions.dto.PermissionDto;
import com.quyeying.charity.permissions.dto.PermissionsQueryDto;
import com.quyeying.charity.permissions.service.PermissionsService;
import com.quyeying.core.dto.JsonResult;
import com.quyeying.core.dto.PageWrapper;
import com.quyeying.db.model.TPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 权限 controller
 * <p>
 * Created by Watson W on 2016/4/27.
 */
@RestController
@RequestMapping("/permissions")
public class PermissionsCtrl {

  private static final Logger LOGGER = LoggerFactory.getLogger(PermissionsCtrl.class);

  @Resource
  private PermissionsService permissionsService;

  /**
   * 根据主键查询权限信息
   *
   * @param pkid   主键
   * @return JsonResult
   */
  @RequestMapping(value = "/{pkid}", method = RequestMethod.GET)
  public JsonResult permission(@PathVariable Long pkid) {
    return JsonResult.success(permissionsService.permission(pkid));
  }

  /**
   * 查询权限分页信息
   *
   * @param dto    查询参数
   * @param errors 校验信息
   * @return JsonResult
   */
  @RequestMapping(method = RequestMethod.POST)
  public JsonResult permissions(@Valid @RequestBody PermissionsQueryDto dto, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用查询权限信息集合接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用查询权限信息集合接口参数错误", errors.getAllErrors());
    }
    return JsonResult.success(PageWrapper.fromPage(permissionsService.permissions(dto)));
  }

  /**
   * 添加权限信息
   *
   * @param permissionDto 权限数据传输对象
   * @param errors        校验信息
   * @return JsonResult
   */
  @RequestMapping(method = RequestMethod.PUT)
  public JsonResult add(@Valid @RequestBody PermissionDto permissionDto, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用添加权限信息接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用添加权限信息接口参数错误", errors.getAllErrors());
    }
    permissionsService.add(permissionDto);
    return JsonResult.successMsg("添加权限信息成功!");
  }

  /**
   * 修改权限
   *
   * @param pkid        主键
   * @param permissionDto 权限数据传输对象
   * @param errors      校验信息
   * @return JsonResult
   */
  @RequestMapping(value = "/{pkid}", method = RequestMethod.POST)
  public JsonResult editByPrimary(@Valid @PathVariable Long pkid, @Valid @RequestBody PermissionDto permissionDto, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用修改权限信息接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用修改权限信息接口参数错误", errors.getAllErrors());
    }
    permissionDto.setPkid(pkid);
    permissionsService.editByPrimary(permissionDto);
    return JsonResult.successMsg("修改权限信息成功!");
  }

  /**
   * 删除权限信息
   *
   * @param pkid 主键
   * @return JsonResult
   */
  @RequestMapping(value = "/{pkid}", method = RequestMethod.DELETE)
  public JsonResult delete(@PathVariable Long pkid) {
    permissionsService.delete(pkid);
    return JsonResult.successMsg("删除权限信息成功!");
  }

  /**
   * 查询所有菜单
   *
   * @return JsonResult
   */
  @RequestMapping(value = "/menus", method = RequestMethod.GET)
  public JsonResult menus() {
    return JsonResult.success(permissionsService.menus());
  }

}
