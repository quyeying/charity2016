package com.quyeying.charity.menu.controller;

import com.quyeying.charity.menu.dto.MenuQueryDto;
import com.quyeying.charity.menu.service.MenuService;
import com.quyeying.core.dto.JsonResult;
import com.quyeying.core.dto.PageWrapper;
import com.quyeying.db.model.TMenus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 菜单 controller
 * <p>
 * Created by Watson W on 2016/4/27.
 */
@RestController
@RequestMapping("/menu")
public class MenuCtrl {

  private static final Logger LOGGER = LoggerFactory.getLogger(MenuCtrl.class);

  @Resource
  private MenuService menuService;

  /**
   * 根据主键查询菜单信息
   *
   * @param pkid 主键
   * @return JsonResult
   */
  @RequestMapping(value = "/{pkid}", method = RequestMethod.GET)
  public JsonResult menu(@PathVariable Long pkid) {

    return JsonResult.success(menuService.menu(pkid));
  }

  /**
   * 查询菜单分页信息
   *
   * @param dto    查询参数
   * @param errors 校验信息
   * @return JsonResult
   */
  @RequestMapping(method = RequestMethod.POST)
  public JsonResult menus(@Valid @RequestBody MenuQueryDto dto, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用查询菜单信息集合接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用查询菜单信息集合接口参数错误", errors.getAllErrors());
    }
    return JsonResult.success(PageWrapper.fromPage(menuService.menus(dto)));
  }

  /**
   * 添加菜单
   *
   * @param menus  菜单实体
   * @param errors 校验信息
   * @return JsonResult
   */
  @RequestMapping(method = RequestMethod.PUT)
  public JsonResult add(@Valid @RequestBody TMenus menus, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用添加菜单信息接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用添加菜单信息接口参数错误", errors.getAllErrors());
    }
    menuService.add(menus);
    return JsonResult.successMsg("添加菜单信息成功!");
  }

  /**
   * 根据主键修改菜单信息
   *
   * @param pkid   主键
   * @param menus  菜单实体
   * @param errors 校验信息
   * @return JsonResult
   */
  @RequestMapping(value = "/{pkid}", method = RequestMethod.POST)
  public JsonResult editByPrimaryKey(@PathVariable Long pkid, @Valid @RequestBody TMenus menus, Errors errors) {
    if (errors.hasErrors()) {
      LOGGER.debug("调用修改菜单信息接口参数异常:{}", errors.getAllErrors());
      JsonResult.failure("调用修改菜单信息接口参数错误", errors.getAllErrors());
    }
    menus.setPkid(pkid);
    menuService.editByPrimaryKey(menus);
    return JsonResult.successMsg("修改菜单信息成功!");
  }

  @RequestMapping(value = "/{pkid}", method = RequestMethod.DELETE)
  public JsonResult delete(@PathVariable Long pkid){
    menuService.delete(pkid);
    return JsonResult.successMsg("删除菜单信息成功!");
  }

}
