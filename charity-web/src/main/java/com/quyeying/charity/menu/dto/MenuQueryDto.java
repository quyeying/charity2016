package com.quyeying.charity.menu.dto;

import com.quyeying.core.dto.PageParams;

/**
 * 菜单查询对象
 * <p>
 * Created by Watson W on 2016/4/27.
 */
public class MenuQueryDto extends PageParams {

  private static final long serialVersionUID = -4377152594147398645L;

}
