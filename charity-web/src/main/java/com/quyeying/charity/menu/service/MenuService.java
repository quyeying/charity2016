package com.quyeying.charity.menu.service;

import com.github.pagehelper.Page;
import com.quyeying.charity.menu.dto.MenuQueryDto;
import com.quyeying.db.model.TMenus;

/**
 * 菜单 service
 * <p>
 * Created by Watson W on 2016/4/27.
 */
public interface MenuService {

  /**
   * 根据主键查询菜单信息
   *
   * @param pkid 主键
   * @return 菜单信息
   */
  TMenus menu(Long pkid);

  /**
   * 查询菜单分页信息
   *
   * @param dto 查询参数
   * @return 菜单集合
   */
  Page<TMenus> menus(MenuQueryDto dto);

  /**
   * 添加菜单
   *
   * @param menus 菜单实体
   */
  void add(TMenus menus);

  /**
   * 修改菜单
   *
   * @param menus 菜单实体
   */
  void editByPrimaryKey(TMenus menus);

  /**
   * 删除菜单
   *
   * @param pkid 主键
   */
  void delete(Long pkid);
}
