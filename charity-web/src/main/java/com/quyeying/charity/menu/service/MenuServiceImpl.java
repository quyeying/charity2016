package com.quyeying.charity.menu.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.quyeying.charity.menu.dto.MenuQueryDto;
import com.quyeying.db.mapper.TMenusMapper;
import com.quyeying.db.model.TMenus;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 菜单 service
 * <p>
 * Created by Watson W on 2016/4/27.
 */
@Service
public class MenuServiceImpl implements MenuService {

  @Resource
  private TMenusMapper mapper;

  @Override
  public TMenus menu(Long pkid) {
    return mapper.selectByPrimaryKey(pkid);
  }

  @Override
  public Page<TMenus> menus(MenuQueryDto dto) {
    PageHelper.startPage(dto.getPage(), dto.getPageSize());
    return (Page<TMenus>) mapper.selectAll();
  }

  @Override
  public void add(TMenus menus) {
    menus.setCreateTime(new Date());
    mapper.insert(menus);
  }

  @Override
  public void editByPrimaryKey(TMenus menus) {
    mapper.updateByPrimaryKeySelective(menus);
  }

  @Override
  public void delete(Long pkid) {
    mapper.deleteByPrimaryKey(pkid);
  }

}
