package com.quyeying.db.model;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "t_permissions_menu")
public class TPermissionsMenu implements Serializable {
  private static final long serialVersionUID = -5662522145052809136L;
  /**
   * 权限标识
   */
  @Column(name = "permissions_id")
  private Long permissionsId;
  /**
   * 菜单标识
   */
  @Column(name = "menu_id")
  private Long menuId;

  /**
   * 获取权限标识
   *
   * @return permissions_id - 权限标识
   */
  public Long getPermissionsId() {
    return permissionsId;
  }

  /**
   * 设置权限标识
   *
   * @param permissionsId 权限标识
   */
  public void setPermissionsId(Long permissionsId) {
    this.permissionsId = permissionsId;
  }

  /**
   * 获取菜单标识
   *
   * @return menu_id - 菜单标识
   */
  public Long getMenuId() {
    return menuId;
  }

  /**
   * 设置菜单标识
   *
   * @param menuId 菜单标识
   */
  public void setMenuId(Long menuId) {
    this.menuId = menuId;
  }
}
