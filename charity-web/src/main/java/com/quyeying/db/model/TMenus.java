package com.quyeying.db.model;

import com.quyeying.core.security.Permission;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_menus")
public class TMenus implements Serializable, Permission {
  private static final long serialVersionUID = -2122948013319575617L;
  /**
   * 主键
   */
  @Id
  private Long pkid;
  /**
   * 父级标识
   */
  @Column(name = "menu_pid")
  private Long menuPid;
  /**
   * 菜单名称
   */
  @Column(name = "menu_name")
  private String menuName;
  @Column(name = "menu_sign")
  private String menuSign;
  /**
   * 菜单路径
   */
  @Column(name = "menu_path")
  private String menuPath;
  @Column(name = "menu_order")
  private Integer menuOrder;
  /**
   * 创建时间
   */
  @Column(name = "create_time")
  private Date createTime;

  /**
   * 获取主键
   *
   * @return pkid - 主键
   */
  public Long getPkid() {
    return pkid;
  }

  /**
   * 设置主键
   *
   * @param pkid 主键
   */
  public void setPkid(Long pkid) {
    this.pkid = pkid;
  }

  /**
   * 获取父级标识
   *
   * @return menu_pid - 父级标识
   */
  public Long getMenuPid() {
    return menuPid;
  }

  /**
   * 设置父级标识
   *
   * @param menuPid 父级标识
   */
  public void setMenuPid(Long menuPid) {
    this.menuPid = menuPid;
  }

  /**
   * 获取菜单名称
   *
   * @return menu_name - 菜单名称
   */
  public String getMenuName() {
    return menuName;
  }

  /**
   * 设置菜单名称
   *
   * @param menuName 菜单名称
   */
  public void setMenuName(String menuName) {
    this.menuName = menuName;
  }

  /**
   * @return menu_sign
   */
  public String getMenuSign() {
    return menuSign;
  }

  /**
   * @param menuSign
   */
  public void setMenuSign(String menuSign) {
    this.menuSign = menuSign;
  }

  /**
   * 获取菜单路径
   *
   * @return menu_path - 菜单路径
   */
  public String getMenuPath() {
    return menuPath;
  }

  /**
   * 设置菜单路径
   *
   * @param menuPath 菜单路径
   */
  public void setMenuPath(String menuPath) {
    this.menuPath = menuPath;
  }

  /**
   * @return menu_order
   */
  public Integer getMenuOrder() {
    return menuOrder;
  }

  /**
   * @param menuOrder
   */
  public void setMenuOrder(Integer menuOrder) {
    this.menuOrder = menuOrder;
  }

  /**
   * 获取创建时间
   *
   * @return create_time - 创建时间
   */
  public Date getCreateTime() {
    return createTime;
  }

  /**
   * 设置创建时间
   *
   * @param createTime 创建时间
   */
  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }
}
