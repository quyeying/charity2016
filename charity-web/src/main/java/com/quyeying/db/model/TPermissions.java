package com.quyeying.db.model;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "t_permissions")
public class TPermissions implements Serializable {
  private static final long serialVersionUID = -2944998754802050103L;
  /**
   * 主键
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long pkid;
  /**
   * 权限名称
   */
  @Column(name = "permission_name")
  private String permissionName;

  /**
   * 获取主键
   *
   * @return pkid - 主键
   */
  public Long getPkid() {
    return pkid;
  }

  /**
   * 设置主键
   *
   * @param pkid 主键
   */
  public void setPkid(Long pkid) {
    this.pkid = pkid;
  }

  /**
   * 获取权限名称
   *
   * @return permission_name - 权限名称
   */
  public String getPermissionName() {
    return permissionName;
  }

  /**
   * 设置权限名称
   *
   * @param permissionName 权限名称
   */
  public void setPermissionName(String permissionName) {
    this.permissionName = permissionName;
  }
}
