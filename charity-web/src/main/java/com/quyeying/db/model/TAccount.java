package com.quyeying.db.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.quyeying.core.security.Account;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "t_account")
@JsonIgnoreProperties(value = {"passWord"})
public class TAccount implements Serializable, Account {
  private static final long serialVersionUID = -865590823176854114L;
  /**
   * 主键
   */
  @Id
  private Long pkid;
  /**
   * 权限标识
   */
  @Column(name = "permissions_id")
  private Long permissionsId;
  /**
   * 用户名称
   */
  @Column(name = "user_name")
  private String userName;
  /**
   * 密码
   */
  @Column(name = "pass_word")
  private String passWord;
  /**
   * 昵称
   */
  @Column(name = "nike_name")
  private String nikeName;
  /**
   * 用户组
   */
  @Column(name = "user_group")
  private String userGroup;

  /**
   * 获取主键
   *
   * @return pkid - 主键
   */
  public Long getPkid() {
    return pkid;
  }

  /**
   * 设置主键
   *
   * @param pkid 主键
   */
  public void setPkid(Long pkid) {
    this.pkid = pkid;
  }

  /**
   * 获取权限标识
   *
   * @return permissions_id - 权限标识
   */
  public Long getPermissionsId() {
    return permissionsId;
  }

  /**
   * 设置权限标识
   *
   * @param permissionsId 权限标识
   */
  public void setPermissionsId(Long permissionsId) {
    this.permissionsId = permissionsId;
  }

  /**
   * 获取用户名称
   *
   * @return user_name - 用户名称
   */
  public String getUserName() {
    return userName;
  }

  /**
   * 设置用户名称
   *
   * @param userName 用户名称
   */
  public void setUserName(String userName) {
    this.userName = userName;
  }

  /**
   * 获取密码
   *
   * @return pass_word - 密码
   */
  public String getPassWord() {
    return passWord;
  }

  /**
   * 设置密码
   *
   * @param passWord 密码
   */
  public void setPassWord(String passWord) {
    this.passWord = passWord;
  }

  /**
   * 获取昵称
   *
   * @return nike_name - 昵称
   */
  public String getNikeName() {
    return nikeName;
  }

  /**
   * 设置昵称
   *
   * @param nikeName 昵称
   */
  public void setNikeName(String nikeName) {
    this.nikeName = nikeName;
  }

  /**
   * 获取用户组
   *
   * @return user_group - 用户组
   */
  public String getUserGroup() {
    return userGroup;
  }

  /**
   * 设置用户组
   *
   * @param userGroup 用户组
   */
  public void setUserGroup(String userGroup) {
    this.userGroup = userGroup;
  }
}
