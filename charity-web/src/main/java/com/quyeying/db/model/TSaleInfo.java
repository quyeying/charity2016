package com.quyeying.db.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_sale_info")
public class TSaleInfo implements Serializable {
    /**
     * 主键
     */
    @Id
    private Long pkid;

    /**
     * 商品id
     */
    @Column(name = "goods_id")
    private Long goodsId;

    /**
     * 售出件数
     */
    @Column(name = "sale_count")
    private Integer saleCount;

    /**
     * 售出总价
     */
    @Column(name = "sale_money")
    private Integer saleMoney;

    /**
     * 订单类型 0-补差订单 1-正常订单
     */
    @Column(name = "sale_type")
    private String saleType;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建用户
     */
    @Column(name = "create_user")
    private Long createUser;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 更新用户
     */
    @Column(name = "update_user")
    private Long updateUser;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return pkid - 主键
     */
    public Long getPkid() {
        return pkid;
    }

    /**
     * 设置主键
     *
     * @param pkid 主键
     */
    public void setPkid(Long pkid) {
        this.pkid = pkid;
    }

    /**
     * 获取商品id
     *
     * @return goods_id - 商品id
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 设置商品id
     *
     * @param goodsId 商品id
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 获取售出件数
     *
     * @return sale_count - 售出件数
     */
    public Integer getSaleCount() {
        return saleCount;
    }

    /**
     * 设置售出件数
     *
     * @param saleCount 售出件数
     */
    public void setSaleCount(Integer saleCount) {
        this.saleCount = saleCount;
    }

    /**
     * 获取售出总价
     *
     * @return sale_money - 售出总价
     */
    public Integer getSaleMoney() {
        return saleMoney;
    }

    /**
     * 设置售出总价
     *
     * @param saleMoney 售出总价
     */
    public void setSaleMoney(Integer saleMoney) {
        this.saleMoney = saleMoney;
    }

    /**
     * 获取订单类型 0-补差订单 1-正常订单
     *
     * @return sale_type - 订单类型 0-补差订单 1-正常订单
     */
    public String getSaleType() {
        return saleType;
    }

    /**
     * 设置订单类型 0-补差订单 1-正常订单
     *
     * @param saleType 订单类型 0-补差订单 1-正常订单
     */
    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建用户
     *
     * @return create_user - 创建用户
     */
    public Long getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建用户
     *
     * @param createUser 创建用户
     */
    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新用户
     *
     * @return update_user - 更新用户
     */
    public Long getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置更新用户
     *
     * @param updateUser 更新用户
     */
    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}
