package com.quyeying.db.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "t_goods")
public class TGoods implements Serializable {
    /**
     * 主键
     */
    @Id
    private String pkid;

    /**
     * 组id
     */
    @Column(name = "group_id")
    private String groupId;

    /**
     * 商品编号
     */
    @Column(name = "goods_num")
    private Integer goodsNum;

    /**
     * 捐赠者
     */
    @Column(name = "person_name")
    private String personName;

    /**
     * 捐赠者联系电话
     */
    @Column(name = "person_phone")
    private String personPhone;

    /**
     * 捐品名称
     */
    @Column(name = "goods_name")
    private String goodsName;

    /**
     * 捐品数量
     */
    @Column(name = "goods_count")
    private Integer goodsCount;

    /**
     * 捐品单价
     */
    @Column(name = "goods_price")
    private Integer goodsPrice;

    /**
     * 是否退回  0-不退回  1-退回
     */
    @Column(name = "goods_return")
    private String goodsReturn;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建用户
     */
    @Column(name = "create_user")
    private Long createUser;

    /**
     * 备注信息
     */
    private String remark;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return pkid - 主键
     */
    public String getPkid() {
        return pkid;
    }

    /**
     * 设置主键
     *
     * @param pkid 主键
     */
    public void setPkid(String pkid) {
        this.pkid = pkid;
    }

    /**
     * 获取组id
     *
     * @return group_id - 组id
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * 设置组id
     *
     * @param groupId 组id
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * 获取商品编号
     *
     * @return goods_num - 商品编号
     */
    public Integer getGoodsNum() {
        return goodsNum;
    }

    /**
     * 设置商品编号
     *
     * @param goodsNum 商品编号
     */
    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    /**
     * 获取捐赠者
     *
     * @return person_name - 捐赠者
     */
    public String getPersonName() {
        return personName;
    }

    /**
     * 设置捐赠者
     *
     * @param personName 捐赠者
     */
    public void setPersonName(String personName) {
        this.personName = personName;
    }

    /**
     * 获取捐赠者联系电话
     *
     * @return person_phone - 捐赠者联系电话
     */
    public String getPersonPhone() {
        return personPhone;
    }

    /**
     * 设置捐赠者联系电话
     *
     * @param personPhone 捐赠者联系电话
     */
    public void setPersonPhone(String personPhone) {
        this.personPhone = personPhone;
    }

    /**
     * 获取捐品名称
     *
     * @return goods_name - 捐品名称
     */
    public String getGoodsName() {
        return goodsName;
    }

    /**
     * 设置捐品名称
     *
     * @param goodsName 捐品名称
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * 获取捐品数量
     *
     * @return goods_count - 捐品数量
     */
    public Integer getGoodsCount() {
        return goodsCount;
    }

    /**
     * 设置捐品数量
     *
     * @param goodsCount 捐品数量
     */
    public void setGoodsCount(Integer goodsCount) {
        this.goodsCount = goodsCount;
    }

    /**
     * 获取捐品单价
     *
     * @return goods_price - 捐品单价
     */
    public Integer getGoodsPrice() {
        return goodsPrice;
    }

    /**
     * 设置捐品单价
     *
     * @param goodsPrice 捐品单价
     */
    public void setGoodsPrice(Integer goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    /**
     * 获取是否退回  0-不退回  1-退回
     *
     * @return goods_return - 是否退回  0-不退回  1-退回
     */
    public String getGoodsReturn() {
        return goodsReturn;
    }

    /**
     * 设置是否退回  0-不退回  1-退回
     *
     * @param goodsReturn 是否退回  0-不退回  1-退回
     */
    public void setGoodsReturn(String goodsReturn) {
        this.goodsReturn = goodsReturn;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建用户
     *
     * @return create_user - 创建用户
     */
    public Long getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建用户
     *
     * @param createUser 创建用户
     */
    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    /**
     * 获取备注信息
     *
     * @return remark - 备注信息
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注信息
     *
     * @param remark 备注信息
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}