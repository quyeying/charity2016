package com.quyeying.db.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "t_group")
public class TGroup implements Serializable {
  private static final long serialVersionUID = -4616336618630195042L;
  /**
   * 主键
   */
  @Id
  private String pkid;

  /**
   * 分组名称
   */
  @Column(name = "group_name")
  private String groupName;

  /**
   * 分组金额
   */
  @Column(name = "group_amount")
  private Integer groupAmount;

  /**
   * 是否自定义金额
   */
  @Column(name = "custom_amount")
  private String customAmount;

  /**
   * 是否退回
   */
  @Column(name = "goods_return")
  private String goodsReturn;

  /**
   * 获取主键
   *
   * @return pkid - 主键
   */
  public String getPkid() {
    return pkid;
  }

  /**
   * 设置主键
   *
   * @param pkid 主键
   */
  public void setPkid(String pkid) {
    this.pkid = pkid;
  }

  /**
   * 获取分组名称
   *
   * @return group_name - 分组名称
   */
  public String getGroupName() {
    return groupName;
  }

  /**
   * 设置分组名称
   *
   * @param groupName 分组名称
   */
  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  /**
   * 获取分组金额
   *
   * @return group_amount - 分组金额
   */
  public Integer getGroupAmount() {
    return groupAmount;
  }

  /**
   * 设置分组金额
   *
   * @param groupAmount 分组金额
   */
  public void setGroupAmount(Integer groupAmount) {
    this.groupAmount = groupAmount;
  }

  /**
   * 获取是否自定义金额
   *
   * @return custom_amount - 是否自定义金额
   */
  public String getCustomAmount() {
    return customAmount;
  }

  /**
   * 设置是否自定义金额
   *
   * @param customAmount 是否自定义金额
   */
  public void setCustomAmount(String customAmount) {
    this.customAmount = customAmount;
  }

  /**
   * 获取是否自定义金额
   *
   * @return goods_return - 是否退回
   */
  public String getGoodsReturn() {
    return goodsReturn;
  }

  /**
   * 设置是否自定义金额
   *
   * @param goodsReturn 是否退回
   */
  public void setGoodsReturn(String goodsReturn) {
    this.goodsReturn = goodsReturn;
  }
}
