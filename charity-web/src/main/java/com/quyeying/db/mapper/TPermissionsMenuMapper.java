package com.quyeying.db.mapper;

import com.quyeying.db.model.TPermissionsMenu;
import tk.mybatis.mapper.common.Mapper;

public interface TPermissionsMenuMapper extends Mapper<TPermissionsMenu> {

  void deleteAllPrimaryKey(Long pkid);

}
