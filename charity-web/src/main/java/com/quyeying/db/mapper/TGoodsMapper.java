package com.quyeying.db.mapper;

import com.quyeying.charity.goods.dto.GoodsDto;
import com.quyeying.db.model.TGoods;
import tk.mybatis.mapper.common.Mapper;

public interface TGoodsMapper extends Mapper<TGoods> {

  /**
   * 保存或者更新商品信息
   */
  void saveOrUpdateGoodsInfo(TGoods goods);

  /**
   * 保存或者更新商品基本信息
   */
  void saveOrUpdateGoodsBaseInfo(TGoods goods);

  /**
   * 根据主键查询商品信息
   * @param pkid 主键
   * @return 商品信息
   */
  GoodsDto selectGoodsInfoByPkid(String pkid);
}
