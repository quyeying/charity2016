package com.quyeying.db.mapper;

import com.quyeying.charity.user.dto.AccountListDto;
import com.quyeying.charity.user.dto.AccountQueryDto;
import com.quyeying.db.model.TAccount;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TAccountMapper extends Mapper<TAccount> {

  List<AccountListDto> findForList(AccountQueryDto dto);
}
