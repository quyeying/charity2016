package com.quyeying.db.mapper;

import com.quyeying.core.dto.Combox;
import com.quyeying.db.model.TPermissions;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TPermissionsMapper extends Mapper<TPermissions> {
  List<Combox> findCombox();
}
