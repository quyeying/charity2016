package com.quyeying.db.mapper;

import com.quyeying.core.dto.Combox;
import com.quyeying.db.model.TGroup;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TGroupMapper extends Mapper<TGroup> {
  /**
   * 查询分组下拉菜单
   * @return
   */
  List<Combox> findCombox();
}
