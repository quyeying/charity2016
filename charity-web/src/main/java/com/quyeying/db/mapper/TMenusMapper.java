package com.quyeying.db.mapper;

import com.quyeying.db.model.TMenus;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TMenusMapper extends Mapper<TMenus> {
  List<String> findPermissionsByAccount(@Param("pkid") Long pkid);

  List<TMenus> findUserMenusById(@Param("accountId") Long accountId);

  List<TMenus> findAll();
}
