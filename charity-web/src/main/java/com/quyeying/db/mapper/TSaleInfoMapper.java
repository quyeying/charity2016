package com.quyeying.db.mapper;

import com.quyeying.db.model.TSaleInfo;
import tk.mybatis.mapper.common.Mapper;

public interface TSaleInfoMapper extends Mapper<TSaleInfo> {
}