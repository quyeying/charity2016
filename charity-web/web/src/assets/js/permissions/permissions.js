/**
 * 权限菜单
 *
 * Created by Watson W on 2016/5/18.
 */
charity.config(['$stateProvider', function ($stateProvider) {
  $stateProvider.state('permissions', {
    abstract: true,
    url: '/permissions',
    templateUrl: 'views/commons/commons.html'
  }).state('permissions.list', {
    url: '',
    controller: 'pmsController',
    templateUrl: 'views/permissions/list.html'
  }).state('permissions.operation', {
    url: "/operation/:pkid",
    templateUrl: "views/permissions/operation.html",
    controller: 'pmsOpController'
  });
}]).controller('pmsController', ['$scope', '$http', '$state', function ($scope, $http, $state) {
  $scope.index.subTitle = "权限菜单列表";
  $scope.permissions = {
    page: {
      total: 0,
      current: 1,
      pageSize: 10,
      changePage: function (i) {
        $scope.permissions.page.current = i;
        loadData();
      }
    },
    datas: []
  };

  $scope.toOperation = function (pkid) {
    $state.go("permissions.operation", {"pkid": pkid});
  };

  $scope.delete = function (pkid) {
    layer.confirm('确认删除权限？', {
      btn: ['删除', '取消'] //按钮
    }, function () {
      $http.delete('permissions/' + pkid, {cache: false}).then(function (r) {
        var d = r.data;
        layer.alert(d.msg);
        loadData();
      },function (r) {
        var d = r.data;
        layer.alert(d.msg);
        console.log(d.data);
      });
    });
  };

  function loadData() {
    $http.post('permissions', JSON.stringify({
      "page": $scope.permissions.page.current,
      "pageSize": $scope.permissions.page.pageSize
    }), {cache: false}).then(function (r) {
      var d = r.data;
      if (d.success) {
        $scope.permissions.page.current = d.data.pageNum;
        $scope.permissions.page.pageSize = d.data.pageSize;
        $scope.permissions.page.total = d.data.total;
        $scope.permissions.datas = d.data.data;
      } else {
        layer.alert(d.msg);
      }
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  }

  loadData();

}]).controller('pmsOpController', ['$scope', '$http', '$state', "$stateParams", "$validation", function ($scope, $http, $state, $stateParams, $validation) {
  var pkid = $stateParams.pkid;
  $scope.operation = {
    data: undefined
  };
  window.console.log(pkid);
  if (pkid) {
    $scope.index.subTitle = "修改权限";
    $http.get('permissions/' + pkid, {cache: false}).then(function (r) {
      var d = r.data;
      window.console.log(d);
      debugger;
      if (d.success) {
        $scope.operation.data = d.data;
      } else {
        layer.alert(d.msg);
      }
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  } else {
    $scope.index.subTitle = "新增权限";
  }

  $http.get('permissions/menus', {cache: false}).then(function (r) {
    var d = r.data;
    window.console.log(d);
    if (d.success) {
      $scope.operation.menus = d.data;
    } else {
      layer.alert(d.msg);
    }
  },function (r) {
    var d = r.data;
    layer.alert(d.msg);
    console.log(d.data);
  });

  $scope.save = function (form) {
    $validation.validate(form).success(function () {
      var method = "PUT", url = "permissions";
      if (pkid) {
        method = "POST";
        url += "/" + pkid;
      }
      $http({
        method: method,
        url: url,
        data: JSON.stringify($scope.operation.data)
      }).then(function (r) {
        var d = r.data;
        layer.alert(d.msg);
        $state.go("permissions.list");
      },function (r) {
        var d = r.data;
        layer.alert(d.msg);
        console.log(d.data);
      });
    }).error(function () {
      layer.alert('请完整填写后在提交!');
    });
  };

  $scope.cancel = function () {
    $state.go("group.list");
  };

}]);
