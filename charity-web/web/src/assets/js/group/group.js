/**
 * 分组前端逻辑
 *
 * Created by Watson W on 2016/5/6.
 */
charity.config(['$stateProvider', function ($stateProvider) {
  $stateProvider.state('group', {
    abstract: true,
    url: '/group',
    templateUrl: 'views/commons/commons.html'
  }).state('group.list', {
    url: '',
    controller: 'groupController',
    templateUrl: 'views/group/list.html'
  }).state('group.operation', {
    url: "/operation/:pkid",
    templateUrl: "views/group/operation.html",
    controller: 'groupOpController'
  });
}]).controller('groupController', ['$scope', '$http', '$state', function ($scope, $http, $state) {
  $scope.index.subTitle = "分组列表";
  $scope.group = {
    page: {
      total: 0,
      current: 1,
      pageSize: 10,
      changePage: function (i) {
        $scope.group.page.current = i;
        loadData();
      }
    },
    datas: []
  };

  $scope.toOperation = function (pkid) {
    $state.go("group.operation", {"pkid": pkid});
  };

  $scope.delete = function (pkid) {
    layer.confirm('确认删除分组？', {
      btn: ['删除', '取消'] //按钮
    }, function () {
      $http.delete('group/' + pkid, {cache: false}).then(function (r) {
        var d = r.data;
        layer.alert(d.msg);
        loadData();
      },function (r) {
        var d = r.data;
        layer.alert(d.msg);
        console.log(d.data);
      });
    });
  };

  function loadData() {
    $http.post('group', JSON.stringify({
      "page": $scope.group.page.current,
      "pageSize": $scope.group.page.pageSize
    }), {cache: false}).then(function (r) {
      var d = r.data;
      if (d.success) {
        $scope.group.page.current = d.data.pageNum;
        $scope.group.page.pageSize = d.data.pageSize;
        $scope.group.page.total = d.data.total;
        $scope.group.datas = d.data.data;
      } else {
        layer.alert(d.msg);
      }
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  }

  loadData();

}]).filter('trueFalseConvertor', function () {
  var filter = function (input) {
    if (input == 0) {
      return "否";
    } else {
      return "是";
    }
  };
  return filter;
}).controller('groupOpController', ['$scope', '$http', '$state', "$stateParams", function ($scope, $http, $state, $stateParams) {
  var pkid = $stateParams.pkid;
  $scope.operation = {
    data: {customAmount: 0}
  };
  window.console.log(pkid);
  if (pkid) {
    $scope.index.subTitle = "修改分组";
    $http.get('group/' + pkid, {cache: false}).then(function (r) {
      var d = r.data;
      window.console.log(d);
      if (d.success) {
        $scope.operation.data = d.data;
      } else {
        layer.alert(d.msg);
      }
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  } else {
    $scope.index.subTitle = "新增分组";
  }

  $scope.save = function () {
    var method = "PUT", url = "group";
    if (pkid) {
      method = "POST";
      url += "/" + pkid;
    }
    $http({
      method: method,
      url: url,
      data: JSON.stringify($scope.operation.data)
    }).then(function (r) {
      var d = r.data;
      layer.alert(d.msg);
      $state.go("group.list");
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  };

  $scope.customAmountValues = function (val) {
    if (val) {
      $scope.operation.data.groupAmount = undefined;
    }
  };
  $scope.goodsReturnValues = function (val) {
    if (val) {
      $scope.operation.data.goodsReturn = undefined;
    }
  };

  $scope.cancel = function () {
    $state.go("group.list");
  };

}]);
