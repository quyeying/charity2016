/**
 * Created by ben on 2016/4/29/029.
 * 首页
 */
charity.controller('mainController', ['$scope', '$http', '$q', '$window', function ($scope, $http, $q, $window) {
  var deferred = $q.defer();

  $http({
    method: 'GET',
    url: "who",
    cache: false
  }).then(function (r) {
    var d = r.data;
    if (d.success) {
      $window.sessionStorage["account"] = JSON.stringify(d.data);
      $scope.index.account = d.data;
      deferred.resolve();
    }
  },function () {
    deferred.reject();
  });
  deferred.promise.then(function () {
    // 请求当前用户菜单
    $http({
      method: 'GET',
      url: "menus",
      cache: false
    }).then(function (r) {
      var d = r.data;
      if (d.success) {
        $window.sessionStorage["menus"] = JSON.stringify(d.data);
        $scope.index.menus = d.data;
      }
    },function (r) {
      var d = r.data;
      layer.alert(d.msg);
      console.log(d.data);
    });
  });


}]);
