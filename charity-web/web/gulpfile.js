var gulp = require('gulp'),
  del = require('del'),
  cleanCss = require('gulp-clean-css'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  useref = require('gulp-useref'),
  gulpif = require('gulp-if'),
  processhtml = require('gulp-processhtml'),
  sourcemaps = require('gulp-sourcemaps'),
  lazypipe = require('lazypipe');

var config = {
  webapp: '../src/main/webapp',
  src: 'src'
};

gulp.task('clean', function () {
  console.log('run clean');
  return del([config.webapp + '/assets',config.webapp + '/views',config.webapp + '/index.html'], {force: true});

});

gulp.task('views', ['clean'], function(){
  return gulp.src([
    config.src + '/views/**/*'
  ]).pipe(gulp.dest(config.webapp + '/views/'));
});

gulp.task('images', ['clean'], function(){
  return gulp.src([
    config.src + '/assets/**/*'
  ]).pipe(gulp.dest(config.webapp + '/assets/'));
});

gulp.task('html', ['clean'], function(){
  return gulp.src(config.src + '/*.html')
    .pipe(useref({}, lazypipe().pipe(sourcemaps.init, { loadMaps: true })))
    .pipe(gulpif('*.js', uglify()))
    .pipe(gulpif('*.css', cleanCss()))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(config.webapp));
});


gulp.task('default', ['clean', 'views', 'images', 'html'], function () {
  gulp.watch(config.src + '/views/**/*', views);
  gulp.watch(config.src + '/assets/images/*', images);
  gulp.watch(config.src + '/*.html', html);
  gulp.watch(config.src + '/assets/js/*', html);
  gulp.watch(config.src + '/assets/js/**/*', html);
  gulp.watch(config.src + '/assets/css/*', html);
});

gulp.task('compile', ['clean', 'views', 'images', 'html']);


